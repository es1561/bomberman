{
    "id": "3be0fe64-7212-4fcc-a07b-11717c3ee252",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "f_name",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": true,
    "charset": 0,
    "first": 0,
    "fontName": "Arial",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "d37543e5-a820-4a4a-8abc-a7197a9a7187",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 11,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "07c8e8f5-29fc-44e9-ad4c-8ff5ec799db6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 11,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 101,
                "y": 41
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "07198d2a-f4eb-4133-bc77-a4d6e0aacea4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 11,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 95,
                "y": 41
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "398eab3a-a49b-4e25-9fbc-58c585597ef1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 88,
                "y": 41
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "d15b8279-9842-4714-8f25-cf55059fe922",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 81,
                "y": 41
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "247c27f5-7fb4-4c92-ac45-90ef2c17a300",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 11,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 71,
                "y": 41
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "cd25086d-b942-4319-a0e4-a2baf8e6f827",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 11,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 62,
                "y": 41
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "fe5b481e-d89b-49c0-a737-04510120d9c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 11,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 58,
                "y": 41
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "1e986367-8777-4206-ae3c-f0fccabb66a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 11,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 53,
                "y": 41
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "88a691ae-c867-4eb6-83d7-58b5fa471561",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 11,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 48,
                "y": 41
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "1a69d3cc-2cca-4ebc-982d-c17fbe305a1c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 11,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 106,
                "y": 41
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "ba0f49b1-75df-496a-b6d8-515feaee98e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 41,
                "y": 41
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "5f6936f0-6f98-4e10-b88d-3734afb6d20c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 11,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 30,
                "y": 41
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "59c876f0-0d0c-41c9-b754-dac9af6f1677",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 11,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 25,
                "y": 41
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "b4e0be84-25c0-40ee-9453-df54b5d5ad69",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 11,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 21,
                "y": 41
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "5ed10336-4876-4eae-9944-a3bea69bbf5e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 11,
                "offset": -1,
                "shift": 3,
                "w": 4,
                "x": 15,
                "y": 41
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "deab6777-b0c0-49f5-9b93-428ec90e426e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 8,
                "y": 41
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "a6ed2400-0cc6-45d0-8a2b-9e3b618be0d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 2,
                "y": 41
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "7e4194c6-32c8-46b3-bbdc-082c41559164",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 115,
                "y": 28
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "efe741ec-c6e7-447b-8f68-500c2ec3ef61",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 108,
                "y": 28
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "3992f7bd-6ec5-49f5-8244-a608cf9768be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 101,
                "y": 28
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "307deef6-38e0-4fe8-8227-4d50950c9edd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 34,
                "y": 41
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "96fafd1d-1773-49e5-988c-f8173431a0df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 112,
                "y": 41
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "9e76020f-d1dd-4be1-a36a-f1eaf5178161",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 119,
                "y": 41
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "7d7c4dd0-bcd0-4a02-ae1e-4ded1b0a6aba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 2,
                "y": 54
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "1684783e-6603-4080-88a2-c85fb3df6983",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 41,
                "y": 67
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "c2af4b69-9431-43ce-8cf7-d4bdcd72d380",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 11,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 36,
                "y": 67
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "39fde81e-4575-42b0-9955-960c18ca8d98",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 11,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 31,
                "y": 67
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "68ad69ac-95e4-4830-94bb-388da188eed7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 24,
                "y": 67
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "3d8d4c80-c09b-49f2-81ac-4459eb2c42e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 17,
                "y": 67
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "aa7f6dab-4265-4d33-a701-22b18d31056e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 10,
                "y": 67
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "62ab6dd2-3bf2-4e09-9ae0-f40310161ed6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 11,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 2,
                "y": 67
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "fd418201-d600-42e2-bb08-841e8fd2d936",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 11,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 115,
                "y": 54
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "a751e7fc-acbb-42cd-96f7-1180bf9194e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 11,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 106,
                "y": 54
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "9f7424bf-f72b-428a-b21e-52bb9e6d1f9c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 11,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 97,
                "y": 54
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "a319bc61-cd78-4841-a10f-5cc3789f2ce7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 11,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 88,
                "y": 54
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "41a28d1d-ffbd-421d-b437-9aa17430ff62",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 11,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 79,
                "y": 54
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "7cb9be6c-06e1-49c9-872c-ba5a9a658378",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 11,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 71,
                "y": 54
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "a3f344c4-906d-4c8d-9909-5f01cfb361ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 11,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 63,
                "y": 54
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "72f7740c-7e3e-4403-b241-93f41133c4c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 11,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 54,
                "y": 54
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "2f6b3683-daf7-4179-8fc8-373dece827c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 11,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 46,
                "y": 54
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "34b63837-6682-4989-8ea8-75d50e29e20a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 11,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 42,
                "y": 54
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "d4bd4375-525a-482b-9b08-ecabc2d0c336",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 35,
                "y": 54
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "34a67f7d-e44a-487d-a54d-a8567f3eb966",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 11,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 26,
                "y": 54
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "68261ac5-226d-47e7-8acc-e1aefd525942",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 11,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 18,
                "y": 54
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "cd9e586b-0c0a-4a7e-95de-d06d48a7ca32",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 11,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 9,
                "y": 54
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "d71ae9f4-b8e3-41d8-a85b-2389e1fadf94",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 11,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 93,
                "y": 28
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "1802329b-aa6e-4f88-8db3-73f7007a2386",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 11,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 84,
                "y": 28
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "2e89aa7b-04c4-4366-8178-947084477e29",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 11,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 76,
                "y": 28
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "c15a0cb2-34de-4c22-8ff6-edd311f9212b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 11,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 40,
                "y": 15
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "a1cbd243-d8dd-4e83-b4ac-d24d8982bb4a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 11,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 26,
                "y": 15
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "cb7cfa97-2101-4fe8-a330-0956af985874",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 11,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 18,
                "y": 15
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "2e68a627-4b42-40fc-9b2b-f087a2a48b47",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 11,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 10,
                "y": 15
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "59639ff7-453f-4079-bc69-cbfed39eea05",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 11,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 2,
                "y": 15
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "039d2f40-2ab5-472c-8644-f29bd72f21c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 11,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 118,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "cf85df38-8ecb-4fc9-86ae-b379ea79b9a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 11,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 107,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "9a1618ca-d38f-401c-a4b1-2a65f64cd934",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 11,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 99,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "586d47a1-9c49-4939-bfd3-33635a3e1cd7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 11,
                "offset": -1,
                "shift": 6,
                "w": 8,
                "x": 89,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "7b33556e-bae5-402f-b143-0021ac9e7aee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 11,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 81,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "043797c4-6e35-4d22-861a-4e7f71153d23",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 11,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 35,
                "y": 15
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "b032c3b0-dee7-40dd-bdf5-aaaad4a27c38",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 11,
                "offset": -1,
                "shift": 3,
                "w": 4,
                "x": 75,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "42934ddf-0d3f-42f4-ade3-6122ebc5e5e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 11,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 64,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "1dbd7efb-1799-4fd5-a617-cf4d2d03a8f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 57,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "bff23911-a7a5-4b69-a2ae-f28fa1a8e9cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 11,
                "offset": -1,
                "shift": 5,
                "w": 7,
                "x": 48,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "9b5f7ef1-3529-4558-bb2e-a38c50f1ba65",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 11,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 43,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "907df282-8282-4e5c-83bf-7b59ca0eb005",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 36,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "1962c529-bc1d-44da-94ec-9209bb62f5b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 11,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 28,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "991d5dea-ef39-4824-ae1f-3de00c509911",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 21,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "c4f9cf3f-a731-4faa-901a-8c6be6b0d36e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 11,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 14,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "a2a445c7-18f8-4c65-bbd7-7fb889969042",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 7,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "1148b488-7241-4f97-9722-ea27d0b74a1b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 11,
                "offset": 0,
                "shift": 3,
                "w": 4,
                "x": 69,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "0f76618b-6802-4e99-902f-aae34f6f03e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 11,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 49,
                "y": 15
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "b8099b0a-c390-4c0e-9f1d-ec19fd8ecdaf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 11,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 120,
                "y": 15
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "ccbe9124-b9b9-4a53-9f84-0ee4fbf0bf54",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 11,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 56,
                "y": 15
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "9262c2df-d498-439e-a784-9c713c9f06a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 11,
                "offset": -1,
                "shift": 3,
                "w": 3,
                "x": 64,
                "y": 28
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "b77309d8-7bab-41d3-afd8-8e26b33c1b6b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 57,
                "y": 28
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "41a505af-613a-4cd8-9c1c-f79311932955",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 11,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 53,
                "y": 28
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "c109e868-dcdd-4b88-b086-675d82428969",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 11,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 43,
                "y": 28
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "7ecb9b55-cc7c-4e3c-8688-aa161617da9d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 11,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 36,
                "y": 28
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "3614bd0e-4318-46aa-bde5-f1ac6aa15cac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 11,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 28,
                "y": 28
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "0f8526e4-f709-473a-b6d1-adb4421bba22",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 11,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 20,
                "y": 28
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "01d52617-d3fb-4263-9287-f251a12123a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 11,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 13,
                "y": 28
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "574c813e-ad96-48df-b051-456c5048e35b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 11,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 7,
                "y": 28
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "03c0d205-bd2a-47c3-888f-820c04712841",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 69,
                "y": 28
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "d5c49202-0b4c-4a35-a747-6ab70e29ab52",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 11,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 2,
                "y": 28
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "f37a079a-af0a-4978-b280-0730bae6b187",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 11,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 113,
                "y": 15
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "07e7c426-ea4e-4a6f-95ad-33fad536398b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 106,
                "y": 15
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "013f8e7f-5e0b-4698-adb4-261288d3a689",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 11,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 97,
                "y": 15
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "fb9cdfbf-e555-4f46-98b2-6ec2a2d97bf9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 90,
                "y": 15
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "12a7cbd4-6910-4daa-9284-ed30cae31a5c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 83,
                "y": 15
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "b08af2d0-9999-4d63-be3c-a247821b0520",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 76,
                "y": 15
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "ab0840b8-b54d-4b68-97ee-687a1b882dd8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 11,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 70,
                "y": 15
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "84a30bc4-9cc2-4370-a4e1-6b4093504ffa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 11,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 66,
                "y": 15
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "6f010288-7532-4384-945d-a65f926aa315",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 11,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 60,
                "y": 15
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "191aa5f2-17fe-4611-93ad-4ad50519a998",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 11,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 48,
                "y": 67
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "9c2a5400-9940-4f65-b894-88df7901af65",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 11,
                "offset": 1,
                "shift": 9,
                "w": 6,
                "x": 55,
                "y": 67
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 7,
    "styleName": "Bold",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}