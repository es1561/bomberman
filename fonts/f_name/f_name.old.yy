{
    "id": "3be0fe64-7212-4fcc-a07b-11717c3ee252",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "f_name",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": true,
    "charset": 0,
    "first": 0,
    "fontName": "Arial",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "4a18929d-af5e-4477-9642-db871d31234e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 9,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "0abb2827-c37a-4de6-b331-7b2e9de92d7e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 9,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 80,
                "y": 35
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "82798243-8e51-4f1a-8f04-6296b434d210",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 74,
                "y": 35
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "36ae263b-af7e-4994-a7fc-1646eb5da45b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 67,
                "y": 35
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "9d3d7a4f-ce8b-4adc-a186-408531ffbfa5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 60,
                "y": 35
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "54d5527c-4841-42f5-b788-26cd96fc5f71",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 9,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 51,
                "y": 35
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "c891190a-6ee4-4674-8f0b-ef2d706880cc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 43,
                "y": 35
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "35a48725-0200-48d4-bdb3-f016e876e17c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 9,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 39,
                "y": 35
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "f1de7bec-d376-4a7e-b4f9-8376e7860fc8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 9,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 34,
                "y": 35
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "3a2d2dfa-029a-47c2-929c-a61bedca3d60",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 9,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 29,
                "y": 35
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "2227ef27-d6d3-4c68-b66b-e61a5de8e149",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 9,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 84,
                "y": 35
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "56d0c9f6-0872-44ce-8c86-fef1eacd89d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 22,
                "y": 35
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "6c9e4789-9cb5-42a2-8119-ebd62930cb0a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 9,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 11,
                "y": 35
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "baf06de1-514d-4fa0-a673-87f0b35710f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 9,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 6,
                "y": 35
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "9a18ead6-8282-4130-b77d-b6e8083321a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 9,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 2,
                "y": 35
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "890dd225-5dfa-4249-82f6-f5f645457fdf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 9,
                "offset": -1,
                "shift": 2,
                "w": 4,
                "x": 119,
                "y": 24
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "23b64fec-bac1-4d56-a864-9a93f28c0d8c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 112,
                "y": 24
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "2a046a93-e35f-42a5-9981-f089586a04d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 106,
                "y": 24
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "b091ae2a-cf23-46e1-8428-a61ee8b7428c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 99,
                "y": 24
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "9adba455-6442-4efc-bd68-d78ee8dc2d47",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 92,
                "y": 24
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "95b8b0ed-cf6a-4fb4-b36a-241ea2547662",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 85,
                "y": 24
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "f7a4b9f7-4338-4341-9044-d18928459486",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 15,
                "y": 35
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "27674eae-2d42-49d9-9d94-97e78c5fa156",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 89,
                "y": 35
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "2bdd2a16-da9c-45c1-be01-ff0c68471fb5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 96,
                "y": 35
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "a59181cb-6905-427f-ba7a-f0eab34aeb42",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 103,
                "y": 35
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "6a9dc7ed-449e-47e3-95e7-5d2a404676b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 6,
                "y": 57
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "47c5eb70-f481-4995-a52a-f6da60f3cb25",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 9,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 2,
                "y": 57
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "c15ce81f-27f3-45dc-8dd3-030025bf9588",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 9,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 120,
                "y": 46
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "8bdad2db-dcaa-4ad9-9f2e-742023bc5275",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 113,
                "y": 46
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "35d40314-f4f8-4109-b63d-601445cc9a1b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 106,
                "y": 46
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "323449e0-f1bf-438a-9c43-7e1bc31cc4c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 99,
                "y": 46
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "59827ff0-abbb-43cc-8d13-ff3a3271e359",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 92,
                "y": 46
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "944d142b-3cc1-419f-a2ed-7b033b1a52f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 9,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 82,
                "y": 46
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "27e5582e-b667-41fd-a3ca-4794406e289e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 74,
                "y": 46
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "ba2926df-d818-4a9c-9329-59a6da3310e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 66,
                "y": 46
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "8ff225f0-cf36-4587-95fa-c9bea86c1f03",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 58,
                "y": 46
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "7d4676cf-1e90-4281-b29f-71bdd2c821b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 50,
                "y": 46
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "8d5bffb4-b602-4def-9007-6198d2337972",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 43,
                "y": 46
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "1e97e9b4-3b89-483d-8281-fd17d11f1147",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 36,
                "y": 46
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "21ad3e14-9ee0-41f4-b325-bf0f625c97c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 28,
                "y": 46
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "1fd140c3-74e7-42ab-96ea-c14a5e981d19",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 20,
                "y": 46
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "d93bcbf0-003c-434d-b19f-bb170ebbc49f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 9,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 16,
                "y": 46
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "59b91b50-a666-4ac9-9dc0-948d3f1b4415",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 10,
                "y": 46
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "6459b1ac-af60-4a75-a2de-775ee18ad0cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 2,
                "y": 46
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "99d82d70-1599-4946-af3a-1bd773005bdb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 119,
                "y": 35
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "3627ca38-ac8e-46d3-9f5d-6c10037a28c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 9,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 110,
                "y": 35
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "612c2f9b-f941-4ffa-813c-f90b2c739a5a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 77,
                "y": 24
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "6f3abdaa-a1ac-4cb1-ad60-643292448a6b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 69,
                "y": 24
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "6c7c3508-14c1-47b0-8a76-a41b47661349",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 62,
                "y": 24
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "09573832-4623-4382-b254-39941d06507e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 29,
                "y": 13
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "c896bce7-5ab2-4ece-961e-c2f4af90d1a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 16,
                "y": 13
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "8aa91533-f2e4-4877-b055-50056e0ea244",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 9,
                "y": 13
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "1f84cdae-e62c-4337-b37e-a67f0df406e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 2,
                "y": 13
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "1c21fbba-8ab3-48a7-a5cb-a94fe522557f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 118,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "d10d67a4-7606-4cf9-92d1-38bd7a291c44",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 6,
                "x": 110,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "d9ac50e5-2fac-438c-8a12-b4a0294e0e35",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 9,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 100,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "d6d515fb-7e0c-4bf5-85a8-60d99cd57afa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 6,
                "x": 92,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "e221abf7-ea97-453a-86d5-ce1712455bda",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 9,
                "offset": -1,
                "shift": 5,
                "w": 7,
                "x": 83,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "ef8c0bd5-b756-4a33-b569-f7f79a369a7c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 76,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "91a95673-39fa-4dd8-8d13-e7ee74b61688",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 9,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 24,
                "y": 13
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "b29934d7-f21b-483f-90eb-bb79c1542bae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 9,
                "offset": -1,
                "shift": 2,
                "w": 4,
                "x": 70,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "008cb8c7-612e-4aaa-84bd-2577aa63b874",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 9,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 60,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "5a2e7b42-2da6-4a44-b8b3-dbc83d1ef938",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 53,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "0594ae80-469d-4d97-88dc-062a8479ef37",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 9,
                "offset": -1,
                "shift": 4,
                "w": 6,
                "x": 45,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "95e647b1-0dbc-4a06-9e90-fffb07890682",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 9,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 41,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "f1893425-8a4e-4c01-8f85-3a20d60c6957",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 34,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "67971952-e6f6-4773-8170-bf9748753d68",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 27,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "2c10648e-db94-4d9b-958e-9b2897403dc3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 20,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "af47b335-3b4f-4793-ac98-5e69deba359d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 13,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "c743a867-cfd8-4d88-8865-dacf05248687",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 6,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "e61c382a-3bb8-4995-9d94-a6bec1df21e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 9,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 65,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "7bbfec8b-6c3b-457c-80a4-fd2f13e100d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 38,
                "y": 13
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "19d9bc1a-3622-4e5f-8d23-a0af93720574",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 106,
                "y": 13
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "d2cb77c9-d980-4e77-80ec-e53e9c21659b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 9,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 45,
                "y": 13
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "1622929f-1f31-41b3-a46e-8c8306a6a3df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 9,
                "offset": -1,
                "shift": 2,
                "w": 3,
                "x": 50,
                "y": 24
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "49faa3ba-f056-4c5e-b428-c6199fccb9c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 43,
                "y": 24
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "f7af47b3-302c-471b-908c-cfa2944f71bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 9,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 39,
                "y": 24
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "ca8a169b-8a09-4670-9103-9837240c4175",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 9,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 30,
                "y": 24
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "b75fbbd4-452d-43d0-b582-dd159b45b4f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 23,
                "y": 24
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "404429f7-008e-4d98-b0b7-24b8681b08b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 16,
                "y": 24
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "3972ac1f-7014-4a62-871e-5e110e449b69",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 9,
                "y": 24
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "3e922473-620c-4813-bd8e-2806f8d4ebf2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 2,
                "y": 24
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "8c0cc2a1-2859-4d07-9ec7-134feeb14d44",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 9,
                "offset": 0,
                "shift": 3,
                "w": 4,
                "x": 118,
                "y": 13
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "c14744f6-0f52-45f2-9a9e-26cbfe268db2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 55,
                "y": 24
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "b4365d42-4a3c-487e-8209-af28b24055b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 9,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 113,
                "y": 13
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "a9bd98b1-0a42-469c-9d4b-3b50dc707b10",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 99,
                "y": 13
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "ddf07b38-3730-4ded-8b4d-c3b022696485",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 92,
                "y": 13
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "d310ab4e-36d9-4b10-bc25-5ca6cac6c7a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 83,
                "y": 13
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "9c927f90-dbfe-4eeb-95cb-1c1d65e4581d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 76,
                "y": 13
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "02255a92-3f54-4565-98ef-99148e65219b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 69,
                "y": 13
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "c80dc44a-720c-4939-b26a-f1a1a4f7149b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 63,
                "y": 13
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "fe5c9a11-d6ee-43bb-9af7-4130683cf7ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 9,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 58,
                "y": 13
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "39188974-b9bd-4cf3-8ebe-121f40da6463",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 9,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 54,
                "y": 13
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "c9966609-3336-4420-9ab4-818fb238a43c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 9,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 49,
                "y": 13
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "15b7e6a7-92f8-4451-a587-40f6c32c9d6a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 13,
                "y": 57
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "6ee974d5-23a6-4138-9d52-8c7940105f4f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 9,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 20,
                "y": 57
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 6,
    "styleName": "Bold",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}