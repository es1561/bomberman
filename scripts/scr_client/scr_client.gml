///@param buffer
///@param socket_id
///scr_client(buffer, client_id);

var buffer = argument0;
var current_socket = argument1;
var msg = buffer_read(buffer, buffer_u8);

switch(msg)
{
	case msg_position:
		var xx = buffer_read(buffer, buffer_u16);
		var yy = buffer_read(buffer, buffer_u16);
		var sprite = buffer_read(buffer, buffer_u16);
		var index = buffer_read(buffer, buffer_u8);
		var socket = buffer_read(buffer, buffer_u8);
		var obj;
		
		if(!ds_map_exists(ds_instance, socket))
		{
			obj = instance_create_layer(xx, yy, 
							"Instances", obj_instance);
			
			obj.sprite_index = sprite;
			obj.image_index = index;
			obj.socket_id = socket;
			
			ds_instance[? socket] = obj;
		}
		else
		{
			obj = ds_instance[? socket];
			
			obj.x = xx;
			obj.y = yy;
			obj.sprite_index = sprite;
			obj.image_index = index;
			obj.socket_id = socket;
		}
		
	break;
	
	case msg_spawn_bomb:
		var xx = buffer_read(buffer, buffer_u16);
		var yy = buffer_read(buffer, buffer_u16);
		var range = buffer_read(buffer, buffer_u8);
		var flag = buffer_read(buffer, buffer_bool);
		
		var bomb = instance_create_layer(xx, yy, 
		 					"Instnaces", obj_bomb);
		bomb.range = range;
		bomb.f_fake = flag;
	break;
	
	case msg_disconnect:
		if(ds_map_exists(ds_instance, current_socket))
			with(ds_instance[? current_socket])
				instance_destroy();
	break;
	
	case msg_start:
		obj_player.f_in_game = true;
	break;
	
	case msg_spawn_item:
		var xx = buffer_read(buffer, buffer_u8);
		var yy = buffer_read(buffer, buffer_u8);
		var index = buffer_read(buffer, buffer_u8);
		
		if(global.ds_grid[# xx, yy] == FREE)
		{
			var item = instance_create_layer(xx * 16, yy * 16, 
		 					"Instances", obj_item);
			item.image_index = index;
		}
	break;
}