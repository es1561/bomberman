///@param ip
///@param buffer
///scr_boardcast(ip, buffer);

var address = argument0;
var buffer = argument1;
var msg = buffer_read(buffer, buffer_u8);

switch(msg)
{
	case msg_boardcast:
		var server_name = buffer_read(buffer, buffer_string);
		var count = buffer_read(buffer, buffer_u8);
	
		if(!ds_map_exists(ds_server, server_name))
		{
			var obj = instance_create_layer(0, 0, "Instances", obj_btn_server);
			
			with(obj)
			{
				name = server_name
			    players = count;
				ip = address;
				text = name + " - " + string(count) + "\n" + ip;
			}
			
			ds_server[? server_count] = obj;
			
			var key = ds_map_find_first(ds_server);
			for(var i = 0; i < ds_map_size(ds_server); i++)
			{
				obj = ds_server[? key];
				
				obj.x = room_width div 2;
				obj.y = 256 + 64 * i;
				
				obj = ds_map_find_next(ds_server, key);
			}
		}
		else
		{
			with(ds_server[? server_name])
			{
				name = server_name
			    players = count;
				ip = address;
				text = name + " - " + string(count) + "\n" + ip;
			}
		}
	break;
}