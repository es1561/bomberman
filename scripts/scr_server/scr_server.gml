///@param buffer
///@param socket_id
///scr_server(buffer, socket_id);

#macro msg_position 0
#macro msg_spawn_bomb 1
#macro msg_disconnect 2
#macro msg_spawn_item 3
#macro msg_start 4

var buffer = argument0;
var current_socket = argument1;
var msg = buffer_read(buffer, buffer_u8);

switch(msg)
{
 	case msg_position:
		var xx = buffer_read(buffer, buffer_u16);
		var yy = buffer_read(buffer, buffer_u16);
		var sprite = buffer_read(buffer, buffer_u16);
		var index = buffer_read(buffer, buffer_u8);
		var new_buffer = buffer_create(256, buffer_fixed, 1);
		
		buffer_write(new_buffer, buffer_u8, msg_position);
		buffer_write(new_buffer, buffer_u16, xx);
		buffer_write(new_buffer, buffer_u16, yy);
		buffer_write(new_buffer, buffer_u16, sprite);
		buffer_write(new_buffer, buffer_u8, index);
		buffer_write(new_buffer, buffer_u8, current_socket);
		
		with(obj_server_client)
		{
			if(socket_id != current_socket)
				network_send_packet(socket_id, 
						new_buffer, buffer_tell(new_buffer));
		}

	break;
	
	case msg_spawn_bomb:
		var xx = buffer_read(buffer, buffer_u16);
		var yy = buffer_read(buffer, buffer_u16);
		var range = buffer_read(buffer, buffer_u8);
		var new_buffer = buffer_create(256, buffer_fixed, 1);
		
		buffer_write(new_buffer, buffer_u8, msg_spawn_bomb);
		buffer_write(new_buffer, buffer_u16, xx);
		buffer_write(new_buffer, buffer_u16, yy);
		buffer_write(new_buffer, buffer_u8, range);
		buffer_write(new_buffer, buffer_bool, true);
		
		network_send_packet(current_socket, new_buffer, 
									buffer_tell(new_buffer)) ;
									
  		new_buffer = buffer_create(256, buffer_fixed, 1);
		buffer_write(new_buffer, buffer_u8, msg_spawn_bomb);
		buffer_write(new_buffer, buffer_u16, xx);
		buffer_write(new_buffer, buffer_u16, yy);
		buffer_write(new_buffer, buffer_u8, range);
		buffer_write(new_buffer, buffer_bool, false);
		
		with(obj_server_client)
			if(socket_id != current_socket)
				network_send_packet(socket_id, new_buffer, 
									buffer_tell(new_buffer)) ;
	break;
}