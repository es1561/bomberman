{
    "id": "56d5f1d0-c462-4eed-9c06-6e66c07908c8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spt_solid",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cbd5e838-1765-4be1-a0e6-c871961d754e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "56d5f1d0-c462-4eed-9c06-6e66c07908c8",
            "compositeImage": {
                "id": "93276382-b0d2-4827-89ec-5f5dc5e55645",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cbd5e838-1765-4be1-a0e6-c871961d754e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cafc98b2-56c5-403b-84d9-7a05998e03f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cbd5e838-1765-4be1-a0e6-c871961d754e",
                    "LayerId": "f56b129c-140a-4009-a9f7-f22202df9a27"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "f56b129c-140a-4009-a9f7-f22202df9a27",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "56d5f1d0-c462-4eed-9c06-6e66c07908c8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}