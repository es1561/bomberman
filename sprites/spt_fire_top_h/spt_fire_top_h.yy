{
    "id": "2c8ff6d2-fcc9-4fa2-b20d-acd83fb4b7ba",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spt_fire_top_h",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "47e536fc-572c-44df-8fc1-9554680b2e4c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2c8ff6d2-fcc9-4fa2-b20d-acd83fb4b7ba",
            "compositeImage": {
                "id": "0ba43f61-53dd-4a1f-9ecd-6bebde6ff8b6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "47e536fc-572c-44df-8fc1-9554680b2e4c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f1b3934f-1db4-44ed-b52b-0569008624c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "47e536fc-572c-44df-8fc1-9554680b2e4c",
                    "LayerId": "f6c0974c-6b27-4e1d-8501-7b2d006f1b54"
                }
            ]
        },
        {
            "id": "7135d1af-c447-4aff-b6e5-0da3949524f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2c8ff6d2-fcc9-4fa2-b20d-acd83fb4b7ba",
            "compositeImage": {
                "id": "b2c705a1-6662-4678-824d-9c0a1d0b229a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7135d1af-c447-4aff-b6e5-0da3949524f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5fbe9cb4-3945-4b3b-9283-13d195a25499",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7135d1af-c447-4aff-b6e5-0da3949524f1",
                    "LayerId": "f6c0974c-6b27-4e1d-8501-7b2d006f1b54"
                }
            ]
        },
        {
            "id": "120134e6-4d9c-4a74-8019-f9d08f10b42b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2c8ff6d2-fcc9-4fa2-b20d-acd83fb4b7ba",
            "compositeImage": {
                "id": "01cb56ec-781f-42f4-b83b-9a7eae04f196",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "120134e6-4d9c-4a74-8019-f9d08f10b42b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c4c027e8-576b-4902-963c-15948d8145af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "120134e6-4d9c-4a74-8019-f9d08f10b42b",
                    "LayerId": "f6c0974c-6b27-4e1d-8501-7b2d006f1b54"
                }
            ]
        },
        {
            "id": "3c3de48d-d547-4efe-8053-9f910ca14ad2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2c8ff6d2-fcc9-4fa2-b20d-acd83fb4b7ba",
            "compositeImage": {
                "id": "bc2149b5-0d1d-4ad5-997f-763618dfe606",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c3de48d-d547-4efe-8053-9f910ca14ad2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a7d14ad3-6f21-41ac-bfe9-6bd1eba86771",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c3de48d-d547-4efe-8053-9f910ca14ad2",
                    "LayerId": "f6c0974c-6b27-4e1d-8501-7b2d006f1b54"
                }
            ]
        },
        {
            "id": "e318d7b3-ad4c-4091-98ff-b7a92da8d92a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2c8ff6d2-fcc9-4fa2-b20d-acd83fb4b7ba",
            "compositeImage": {
                "id": "5b7db2ae-a381-47b2-af1b-6a9ead10645f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e318d7b3-ad4c-4091-98ff-b7a92da8d92a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "745e411d-1163-4061-a17d-e88ab959a770",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e318d7b3-ad4c-4091-98ff-b7a92da8d92a",
                    "LayerId": "f6c0974c-6b27-4e1d-8501-7b2d006f1b54"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "f6c0974c-6b27-4e1d-8501-7b2d006f1b54",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2c8ff6d2-fcc9-4fa2-b20d-acd83fb4b7ba",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}