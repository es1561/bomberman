{
    "id": "77a41157-72e8-4c0e-b243-bd9672c01a38",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spt_player_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 3,
    "bbox_right": 16,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4973d7e2-4951-44ae-a0c9-acd29cf328f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77a41157-72e8-4c0e-b243-bd9672c01a38",
            "compositeImage": {
                "id": "1c49d49a-8a1c-4da3-9c80-b6f8b30cc2c4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4973d7e2-4951-44ae-a0c9-acd29cf328f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aaac7cfc-a852-47a6-a508-438c1de7b9c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4973d7e2-4951-44ae-a0c9-acd29cf328f5",
                    "LayerId": "6a1e9755-fb3e-45d9-a757-d017728e49e3"
                }
            ]
        },
        {
            "id": "3744e147-08d5-4565-a6b8-f8e38e3bc05e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77a41157-72e8-4c0e-b243-bd9672c01a38",
            "compositeImage": {
                "id": "8bd82d2c-c173-4ba2-a6c1-259115b9b092",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3744e147-08d5-4565-a6b8-f8e38e3bc05e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5822cc7b-4625-4241-b8d1-c3655dbbd673",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3744e147-08d5-4565-a6b8-f8e38e3bc05e",
                    "LayerId": "6a1e9755-fb3e-45d9-a757-d017728e49e3"
                }
            ]
        },
        {
            "id": "3d9ba7c7-88c3-4a3c-97ae-630d232f9661",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77a41157-72e8-4c0e-b243-bd9672c01a38",
            "compositeImage": {
                "id": "e17ab8dc-45ae-40fd-88aa-1c6b45a87881",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d9ba7c7-88c3-4a3c-97ae-630d232f9661",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05bb6710-ad6d-4a6d-bb36-57c500fb4922",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d9ba7c7-88c3-4a3c-97ae-630d232f9661",
                    "LayerId": "6a1e9755-fb3e-45d9-a757-d017728e49e3"
                }
            ]
        },
        {
            "id": "62a13df5-9137-4a91-84c8-732e6ee5c85f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77a41157-72e8-4c0e-b243-bd9672c01a38",
            "compositeImage": {
                "id": "dcaf93a1-03b9-48e7-b191-f079fbac9eed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "62a13df5-9137-4a91-84c8-732e6ee5c85f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a69aee85-9635-4c50-ba81-51ef370a59ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "62a13df5-9137-4a91-84c8-732e6ee5c85f",
                    "LayerId": "6a1e9755-fb3e-45d9-a757-d017728e49e3"
                }
            ]
        },
        {
            "id": "248dcaff-472b-47cf-b61c-5b4d3aa4b8cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77a41157-72e8-4c0e-b243-bd9672c01a38",
            "compositeImage": {
                "id": "58ac49dc-5e63-43e0-8458-2f119438b3c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "248dcaff-472b-47cf-b61c-5b4d3aa4b8cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "18051e14-328d-4b4f-8df5-665a5eadadc7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "248dcaff-472b-47cf-b61c-5b4d3aa4b8cc",
                    "LayerId": "6a1e9755-fb3e-45d9-a757-d017728e49e3"
                }
            ]
        },
        {
            "id": "0617d035-fd31-46a7-a937-15d37044be39",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77a41157-72e8-4c0e-b243-bd9672c01a38",
            "compositeImage": {
                "id": "4a9fd0c8-f6c6-4207-bdfb-1ce378085002",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0617d035-fd31-46a7-a937-15d37044be39",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e816ca61-069c-4068-a012-b7239c4e9962",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0617d035-fd31-46a7-a937-15d37044be39",
                    "LayerId": "6a1e9755-fb3e-45d9-a757-d017728e49e3"
                }
            ]
        },
        {
            "id": "b4ea9cf0-5101-448b-99ed-312a83a70b57",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77a41157-72e8-4c0e-b243-bd9672c01a38",
            "compositeImage": {
                "id": "a9a6b943-fdcb-4b06-afe8-6df2d3f97b93",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4ea9cf0-5101-448b-99ed-312a83a70b57",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f9a87067-f10f-4046-ad29-ff50aa6f7329",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4ea9cf0-5101-448b-99ed-312a83a70b57",
                    "LayerId": "6a1e9755-fb3e-45d9-a757-d017728e49e3"
                }
            ]
        }
    ],
    "gridX": 16,
    "gridY": 16,
    "height": 24,
    "layers": [
        {
            "id": "6a1e9755-fb3e-45d9-a757-d017728e49e3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "77a41157-72e8-4c0e-b243-bd9672c01a38",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 19,
    "xorig": 9,
    "yorig": 23
}