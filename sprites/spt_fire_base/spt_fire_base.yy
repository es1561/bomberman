{
    "id": "5d752ef9-5694-49b6-9da0-bfaf64320e50",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spt_fire_base",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dffab5a8-8127-429e-9d31-1dbdcad765cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d752ef9-5694-49b6-9da0-bfaf64320e50",
            "compositeImage": {
                "id": "6f14db2a-fbe8-458e-8ff0-480c6bb11fe1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dffab5a8-8127-429e-9d31-1dbdcad765cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f909161c-c6bb-467c-8b04-33294394b308",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dffab5a8-8127-429e-9d31-1dbdcad765cf",
                    "LayerId": "c17bf3f2-060c-4289-9d97-48910489e516"
                }
            ]
        },
        {
            "id": "58bde031-88fd-449e-abd2-222c4be79c12",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d752ef9-5694-49b6-9da0-bfaf64320e50",
            "compositeImage": {
                "id": "202f6595-e608-48bd-a7b0-c9cb93780e8d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "58bde031-88fd-449e-abd2-222c4be79c12",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "83cc033c-6763-4b22-89b1-594a1a2b0954",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "58bde031-88fd-449e-abd2-222c4be79c12",
                    "LayerId": "c17bf3f2-060c-4289-9d97-48910489e516"
                }
            ]
        },
        {
            "id": "d8a6b33d-7726-4c01-b460-6696ae48c1ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d752ef9-5694-49b6-9da0-bfaf64320e50",
            "compositeImage": {
                "id": "6c1c2df9-f411-457a-a1ed-c1e4dbc5af34",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d8a6b33d-7726-4c01-b460-6696ae48c1ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f06069c-08cb-4a54-a146-451b087d47b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8a6b33d-7726-4c01-b460-6696ae48c1ea",
                    "LayerId": "c17bf3f2-060c-4289-9d97-48910489e516"
                }
            ]
        },
        {
            "id": "088bc195-da9e-4b9b-bf55-736ddf13e415",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d752ef9-5694-49b6-9da0-bfaf64320e50",
            "compositeImage": {
                "id": "fa2446c2-ccf6-4e08-a826-a5b938273cdf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "088bc195-da9e-4b9b-bf55-736ddf13e415",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9076fed3-af1d-4525-9a95-765ed6c24d11",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "088bc195-da9e-4b9b-bf55-736ddf13e415",
                    "LayerId": "c17bf3f2-060c-4289-9d97-48910489e516"
                }
            ]
        },
        {
            "id": "8cffcb66-067e-4066-bdc0-311c4d3cac2b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d752ef9-5694-49b6-9da0-bfaf64320e50",
            "compositeImage": {
                "id": "7925d211-5eaf-46fd-b394-8d5ed1dbe55d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8cffcb66-067e-4066-bdc0-311c4d3cac2b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7fdfb1f0-2a8d-45c4-a155-c23614bf212a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8cffcb66-067e-4066-bdc0-311c4d3cac2b",
                    "LayerId": "c17bf3f2-060c-4289-9d97-48910489e516"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "c17bf3f2-060c-4289-9d97-48910489e516",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5d752ef9-5694-49b6-9da0-bfaf64320e50",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}