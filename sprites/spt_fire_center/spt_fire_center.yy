{
    "id": "cb03ea11-2de4-42b1-a75c-7b156a586acc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spt_fire_center",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cb1167b3-5042-4d47-bd97-a60ea1356f3a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cb03ea11-2de4-42b1-a75c-7b156a586acc",
            "compositeImage": {
                "id": "ae22fb80-7746-4989-bac9-c1590ade5d2a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cb1167b3-5042-4d47-bd97-a60ea1356f3a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "71eafbfc-0761-4dee-95fd-c0fc45f62fbe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb1167b3-5042-4d47-bd97-a60ea1356f3a",
                    "LayerId": "2258a078-0757-4735-b5e6-17fdfa944646"
                }
            ]
        },
        {
            "id": "d353aa14-7f6d-4b0f-95ac-d9dc796fd4be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cb03ea11-2de4-42b1-a75c-7b156a586acc",
            "compositeImage": {
                "id": "38d8018b-a3d3-4589-9bfd-8a3a10747719",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d353aa14-7f6d-4b0f-95ac-d9dc796fd4be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8305d862-7ef1-4dc6-b8c4-d994961c1333",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d353aa14-7f6d-4b0f-95ac-d9dc796fd4be",
                    "LayerId": "2258a078-0757-4735-b5e6-17fdfa944646"
                }
            ]
        },
        {
            "id": "17cdf35f-fe51-49d6-b2cd-b4a273627faa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cb03ea11-2de4-42b1-a75c-7b156a586acc",
            "compositeImage": {
                "id": "9b6a96e7-9bbb-4af4-a56d-f97ebf020a12",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "17cdf35f-fe51-49d6-b2cd-b4a273627faa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fbc2b00d-2f48-4119-bb7b-6e266ca753d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "17cdf35f-fe51-49d6-b2cd-b4a273627faa",
                    "LayerId": "2258a078-0757-4735-b5e6-17fdfa944646"
                }
            ]
        },
        {
            "id": "5b78f720-2809-43e0-868b-3110ca8e20d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cb03ea11-2de4-42b1-a75c-7b156a586acc",
            "compositeImage": {
                "id": "bca30d84-190f-44ca-978a-885c5aad4402",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b78f720-2809-43e0-868b-3110ca8e20d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "90b9c7b8-25bb-404b-bef3-cffedf53ea29",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b78f720-2809-43e0-868b-3110ca8e20d6",
                    "LayerId": "2258a078-0757-4735-b5e6-17fdfa944646"
                }
            ]
        },
        {
            "id": "fa0dead6-9075-4743-a04a-0a8163e9e527",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cb03ea11-2de4-42b1-a75c-7b156a586acc",
            "compositeImage": {
                "id": "f99528ae-149d-45e4-853c-0b12b198c3fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fa0dead6-9075-4743-a04a-0a8163e9e527",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1422a641-4e2a-4a72-96e0-6750670a5b25",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa0dead6-9075-4743-a04a-0a8163e9e527",
                    "LayerId": "2258a078-0757-4735-b5e6-17fdfa944646"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "2258a078-0757-4735-b5e6-17fdfa944646",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cb03ea11-2de4-42b1-a75c-7b156a586acc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}