{
    "id": "f8c4d9a7-2f4b-491d-8fe0-2f3b138d858c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spt_player_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 2,
    "bbox_right": 16,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3f719940-596c-4f37-91a0-83d99ed930e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8c4d9a7-2f4b-491d-8fe0-2f3b138d858c",
            "compositeImage": {
                "id": "a563e0dc-51f9-41fd-a834-95c32e8b5c9e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f719940-596c-4f37-91a0-83d99ed930e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f63efeaa-9a90-4923-93ff-b75784de94e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f719940-596c-4f37-91a0-83d99ed930e0",
                    "LayerId": "3f1e7acf-29ea-4524-a979-d9ee3e827db1"
                }
            ]
        },
        {
            "id": "61f8156b-5b88-48cf-a064-08da3e67671c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8c4d9a7-2f4b-491d-8fe0-2f3b138d858c",
            "compositeImage": {
                "id": "9840d4dc-fb75-4bb1-8130-1f8365d87a0f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "61f8156b-5b88-48cf-a064-08da3e67671c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2dec5eaa-ec29-40c1-946a-45f128e38f61",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "61f8156b-5b88-48cf-a064-08da3e67671c",
                    "LayerId": "3f1e7acf-29ea-4524-a979-d9ee3e827db1"
                }
            ]
        },
        {
            "id": "73156984-7f3f-4898-aa0f-c0a4a14bba76",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8c4d9a7-2f4b-491d-8fe0-2f3b138d858c",
            "compositeImage": {
                "id": "c0dd85b5-20a7-43bc-bc1e-9c3392c6e76b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "73156984-7f3f-4898-aa0f-c0a4a14bba76",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0dbeb463-3197-46d2-a192-9547f4d3820d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "73156984-7f3f-4898-aa0f-c0a4a14bba76",
                    "LayerId": "3f1e7acf-29ea-4524-a979-d9ee3e827db1"
                }
            ]
        },
        {
            "id": "41a4d41b-6f0e-4a77-9957-fc8e0b93c538",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8c4d9a7-2f4b-491d-8fe0-2f3b138d858c",
            "compositeImage": {
                "id": "58bdd5ef-1ee4-4714-9d05-0b052a0fb3d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "41a4d41b-6f0e-4a77-9957-fc8e0b93c538",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e01b2067-7340-40b8-b1e5-563c4995867c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "41a4d41b-6f0e-4a77-9957-fc8e0b93c538",
                    "LayerId": "3f1e7acf-29ea-4524-a979-d9ee3e827db1"
                }
            ]
        },
        {
            "id": "880c4b2a-5eef-46e0-8a27-60f409006154",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8c4d9a7-2f4b-491d-8fe0-2f3b138d858c",
            "compositeImage": {
                "id": "4962be4d-e6d0-46d1-8d2f-7a645273c557",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "880c4b2a-5eef-46e0-8a27-60f409006154",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4e66be42-3095-4b1b-8488-0e7d09f3928d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "880c4b2a-5eef-46e0-8a27-60f409006154",
                    "LayerId": "3f1e7acf-29ea-4524-a979-d9ee3e827db1"
                }
            ]
        },
        {
            "id": "72e232cc-4d6c-48d1-8959-58b4ac065823",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8c4d9a7-2f4b-491d-8fe0-2f3b138d858c",
            "compositeImage": {
                "id": "b21f7998-f64f-4d7d-bb89-1626bdc7db80",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "72e232cc-4d6c-48d1-8959-58b4ac065823",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2b88bab3-c898-42c1-a8c8-cc970bba8b00",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "72e232cc-4d6c-48d1-8959-58b4ac065823",
                    "LayerId": "3f1e7acf-29ea-4524-a979-d9ee3e827db1"
                }
            ]
        },
        {
            "id": "349a3536-33c8-4983-9f14-e11c506a1fe5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8c4d9a7-2f4b-491d-8fe0-2f3b138d858c",
            "compositeImage": {
                "id": "ce767518-92d5-497e-94d1-993c0d4abd14",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "349a3536-33c8-4983-9f14-e11c506a1fe5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6225d898-b857-426e-9269-cd7fcd8c01e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "349a3536-33c8-4983-9f14-e11c506a1fe5",
                    "LayerId": "3f1e7acf-29ea-4524-a979-d9ee3e827db1"
                }
            ]
        }
    ],
    "gridX": 16,
    "gridY": 16,
    "height": 24,
    "layers": [
        {
            "id": "3f1e7acf-29ea-4524-a979-d9ee3e827db1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f8c4d9a7-2f4b-491d-8fe0-2f3b138d858c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 19,
    "xorig": 9,
    "yorig": 23
}