{
    "id": "b34774bd-27be-42bd-8b93-89762cdb809e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spt_fire_h",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cc6ca007-1f43-4ab3-9975-460db9881114",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b34774bd-27be-42bd-8b93-89762cdb809e",
            "compositeImage": {
                "id": "ca1ebb42-8fa4-4ceb-8583-26b185c1e23d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cc6ca007-1f43-4ab3-9975-460db9881114",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7bdb25a5-9e85-425b-a6d6-e298779da0c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cc6ca007-1f43-4ab3-9975-460db9881114",
                    "LayerId": "d018e509-98a7-4142-967a-823c24e5e8a6"
                }
            ]
        },
        {
            "id": "0d70fbc9-8ea7-424f-aa3a-dd87a70eece7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b34774bd-27be-42bd-8b93-89762cdb809e",
            "compositeImage": {
                "id": "0f32605a-ca0d-4408-9855-338dc62e424c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0d70fbc9-8ea7-424f-aa3a-dd87a70eece7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d21128cc-2151-4263-86b5-175e27bb3ba2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d70fbc9-8ea7-424f-aa3a-dd87a70eece7",
                    "LayerId": "d018e509-98a7-4142-967a-823c24e5e8a6"
                }
            ]
        },
        {
            "id": "c49018a4-a02f-433c-beae-9bb3760b830b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b34774bd-27be-42bd-8b93-89762cdb809e",
            "compositeImage": {
                "id": "e93e41c1-0933-41bf-bee5-4e04eabeb265",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c49018a4-a02f-433c-beae-9bb3760b830b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0bcb4cf6-d870-4331-9f6e-beb850cb77e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c49018a4-a02f-433c-beae-9bb3760b830b",
                    "LayerId": "d018e509-98a7-4142-967a-823c24e5e8a6"
                }
            ]
        },
        {
            "id": "e5a2125f-a54c-4a3d-aaf3-c450f51a186e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b34774bd-27be-42bd-8b93-89762cdb809e",
            "compositeImage": {
                "id": "bba16c61-9a50-40ff-97f8-96b96ec85b1c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e5a2125f-a54c-4a3d-aaf3-c450f51a186e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9d774c1a-5f66-49aa-8031-2ad88f708bae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e5a2125f-a54c-4a3d-aaf3-c450f51a186e",
                    "LayerId": "d018e509-98a7-4142-967a-823c24e5e8a6"
                }
            ]
        },
        {
            "id": "e3b81fa4-2ef7-4807-9949-022ee4d72b09",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b34774bd-27be-42bd-8b93-89762cdb809e",
            "compositeImage": {
                "id": "7913334b-a826-4c02-a45f-256151d95631",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e3b81fa4-2ef7-4807-9949-022ee4d72b09",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f24d789-66b8-4499-9cd2-ce6de0827f60",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3b81fa4-2ef7-4807-9949-022ee4d72b09",
                    "LayerId": "d018e509-98a7-4142-967a-823c24e5e8a6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "d018e509-98a7-4142-967a-823c24e5e8a6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b34774bd-27be-42bd-8b93-89762cdb809e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}