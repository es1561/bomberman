{
    "id": "0dab64bf-e759-46aa-8803-280155787079",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spt_bomb_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 13,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f71dfbb5-381d-461c-b9bc-7d7f94df80cb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0dab64bf-e759-46aa-8803-280155787079",
            "compositeImage": {
                "id": "9f7cb666-a4d8-4e69-83ec-1e6d12463200",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f71dfbb5-381d-461c-b9bc-7d7f94df80cb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57e563b9-89a3-4979-b339-3c81e9fc5281",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f71dfbb5-381d-461c-b9bc-7d7f94df80cb",
                    "LayerId": "3e1c901f-af66-4702-a903-08b0c0ae337d"
                }
            ]
        },
        {
            "id": "89c56059-5dd3-4cb4-932b-3b7e4bfb7498",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0dab64bf-e759-46aa-8803-280155787079",
            "compositeImage": {
                "id": "886ad676-60ea-445a-beff-6c933d650660",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "89c56059-5dd3-4cb4-932b-3b7e4bfb7498",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c5d24c94-5326-4cc7-88d4-ea83547f1901",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "89c56059-5dd3-4cb4-932b-3b7e4bfb7498",
                    "LayerId": "3e1c901f-af66-4702-a903-08b0c0ae337d"
                }
            ]
        },
        {
            "id": "f5f962e7-11aa-4e94-a5b5-8f7f950e9362",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0dab64bf-e759-46aa-8803-280155787079",
            "compositeImage": {
                "id": "f2202585-98f5-48bf-a4b6-4a9ad7e87c8e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f5f962e7-11aa-4e94-a5b5-8f7f950e9362",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "56fa6468-6206-4097-9c13-0409581d86dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f5f962e7-11aa-4e94-a5b5-8f7f950e9362",
                    "LayerId": "3e1c901f-af66-4702-a903-08b0c0ae337d"
                }
            ]
        },
        {
            "id": "5bfc0ce5-b69c-4c6c-96de-9c77c99b6dc8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0dab64bf-e759-46aa-8803-280155787079",
            "compositeImage": {
                "id": "51ea60c2-ab41-4fbd-ad43-a5b5877b49bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5bfc0ce5-b69c-4c6c-96de-9c77c99b6dc8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bfc68987-2ec9-4cad-8596-6e4fbc102f28",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5bfc0ce5-b69c-4c6c-96de-9c77c99b6dc8",
                    "LayerId": "3e1c901f-af66-4702-a903-08b0c0ae337d"
                }
            ]
        },
        {
            "id": "8d391df7-708b-41ba-aacb-9320dead2063",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0dab64bf-e759-46aa-8803-280155787079",
            "compositeImage": {
                "id": "68cd2d24-6365-492b-b983-13d93e0104a8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d391df7-708b-41ba-aacb-9320dead2063",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f440f31-de55-4830-abdb-c7b48dd1ef3f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d391df7-708b-41ba-aacb-9320dead2063",
                    "LayerId": "3e1c901f-af66-4702-a903-08b0c0ae337d"
                }
            ]
        },
        {
            "id": "1f028b9f-d07b-415d-91ea-ed0c639bca70",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0dab64bf-e759-46aa-8803-280155787079",
            "compositeImage": {
                "id": "ae2cc7b8-fa90-4030-8a3c-b339c9be81ef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f028b9f-d07b-415d-91ea-ed0c639bca70",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c2dc9b81-5132-4ef4-bf93-ef351b98dc90",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f028b9f-d07b-415d-91ea-ed0c639bca70",
                    "LayerId": "3e1c901f-af66-4702-a903-08b0c0ae337d"
                }
            ]
        },
        {
            "id": "9d386d0c-073a-4e02-a002-3de1b1d5dcdb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0dab64bf-e759-46aa-8803-280155787079",
            "compositeImage": {
                "id": "077241c5-6969-427f-b936-915436252cb2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d386d0c-073a-4e02-a002-3de1b1d5dcdb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "83e6236c-66f2-44b1-8040-176572c3599a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d386d0c-073a-4e02-a002-3de1b1d5dcdb",
                    "LayerId": "3e1c901f-af66-4702-a903-08b0c0ae337d"
                }
            ]
        }
    ],
    "gridX": 14,
    "gridY": 24,
    "height": 24,
    "layers": [
        {
            "id": "3e1c901f-af66-4702-a903-08b0c0ae337d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0dab64bf-e759-46aa-8803-280155787079",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 9,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 14,
    "xorig": 7,
    "yorig": 23
}