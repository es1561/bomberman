{
    "id": "da9c4d7f-6467-43ba-8493-d1b3d3e5f6f7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spt_player_mask",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 4,
    "bbox_right": 14,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0a581d90-f101-4790-9d9b-d1814b82274a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "da9c4d7f-6467-43ba-8493-d1b3d3e5f6f7",
            "compositeImage": {
                "id": "d43bfad5-a022-48b7-a1b5-e3a341115782",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a581d90-f101-4790-9d9b-d1814b82274a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d33cb204-d5a3-4b1c-b2a1-f2ab2db8d476",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a581d90-f101-4790-9d9b-d1814b82274a",
                    "LayerId": "328ba065-641a-44f6-9042-4aa018941b6a"
                }
            ]
        }
    ],
    "gridX": 1,
    "gridY": 1,
    "height": 24,
    "layers": [
        {
            "id": "328ba065-641a-44f6-9042-4aa018941b6a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "da9c4d7f-6467-43ba-8493-d1b3d3e5f6f7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 65,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 19,
    "xorig": 9,
    "yorig": 23
}