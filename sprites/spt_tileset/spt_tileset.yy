{
    "id": "21f9ae59-9fa4-43d7-92df-301a6d6275e5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spt_tileset",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 16,
    "bbox_right": 47,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2e13f54a-ac27-4e77-80d7-37af0dd8422a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "21f9ae59-9fa4-43d7-92df-301a6d6275e5",
            "compositeImage": {
                "id": "3de5448c-65c2-4909-82d8-705132102d47",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e13f54a-ac27-4e77-80d7-37af0dd8422a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "689cc024-9d7b-4ede-8084-15f049147808",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e13f54a-ac27-4e77-80d7-37af0dd8422a",
                    "LayerId": "c2267c94-533d-47f3-8c90-d20049d43940"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "c2267c94-533d-47f3-8c90-d20049d43940",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "21f9ae59-9fa4-43d7-92df-301a6d6275e5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 0,
    "yorig": 0
}