{
    "id": "04a42be5-7636-4ef9-89bb-28d4967ff21a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spt_button_big",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 43,
    "bbox_left": 0,
    "bbox_right": 185,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7cb6b880-02ed-4b24-aec9-12307bbb8a2a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "04a42be5-7636-4ef9-89bb-28d4967ff21a",
            "compositeImage": {
                "id": "f53ed372-5854-4f67-b30d-53f4d6f45943",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7cb6b880-02ed-4b24-aec9-12307bbb8a2a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "78079e4d-fc3b-4c0d-b7e9-2c3520d366ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7cb6b880-02ed-4b24-aec9-12307bbb8a2a",
                    "LayerId": "27918d8a-6644-40aa-83bf-d5efd5a078a3"
                }
            ]
        }
    ],
    "gridX": 8,
    "gridY": 16,
    "height": 44,
    "layers": [
        {
            "id": "27918d8a-6644-40aa-83bf-d5efd5a078a3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "04a42be5-7636-4ef9-89bb-28d4967ff21a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 186,
    "xorig": 93,
    "yorig": 22
}