{
    "id": "cf630650-02b5-4ef6-8a86-dea4630afa43",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spt_player_side",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 1,
    "bbox_right": 17,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "44d5f219-fb25-405f-9713-9989d68a044b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cf630650-02b5-4ef6-8a86-dea4630afa43",
            "compositeImage": {
                "id": "59e1d8c5-d337-4fd0-abb4-e265d63dd4b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "44d5f219-fb25-405f-9713-9989d68a044b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "843be42b-ccef-4eb1-99d9-dcfa650a2b16",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "44d5f219-fb25-405f-9713-9989d68a044b",
                    "LayerId": "719e19d0-15a6-4654-b9a2-2875171b4cab"
                }
            ]
        },
        {
            "id": "366bf794-9e03-4ec7-804d-f95db21dd22f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cf630650-02b5-4ef6-8a86-dea4630afa43",
            "compositeImage": {
                "id": "e024573b-3533-4508-8871-d7f36cc65e81",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "366bf794-9e03-4ec7-804d-f95db21dd22f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4bc5278f-46e5-430a-aac5-98b1e961ea3b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "366bf794-9e03-4ec7-804d-f95db21dd22f",
                    "LayerId": "719e19d0-15a6-4654-b9a2-2875171b4cab"
                }
            ]
        },
        {
            "id": "837fc132-568b-432d-b1c5-a37e39dd4eea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cf630650-02b5-4ef6-8a86-dea4630afa43",
            "compositeImage": {
                "id": "a85f99a4-88de-4174-be8b-49d987e879d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "837fc132-568b-432d-b1c5-a37e39dd4eea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f1708a2d-1c83-4f07-aad5-370a7916ae3f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "837fc132-568b-432d-b1c5-a37e39dd4eea",
                    "LayerId": "719e19d0-15a6-4654-b9a2-2875171b4cab"
                }
            ]
        },
        {
            "id": "09cdd851-9fb9-4aec-8b08-4ef9a93604a2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cf630650-02b5-4ef6-8a86-dea4630afa43",
            "compositeImage": {
                "id": "1f75e6cc-d2cb-41f7-bedd-51f468ee3e62",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "09cdd851-9fb9-4aec-8b08-4ef9a93604a2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4102b6e2-7e80-44be-97bc-2e90d0064e33",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "09cdd851-9fb9-4aec-8b08-4ef9a93604a2",
                    "LayerId": "719e19d0-15a6-4654-b9a2-2875171b4cab"
                }
            ]
        },
        {
            "id": "085ec14f-7252-42f6-8bda-119afffeac2c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cf630650-02b5-4ef6-8a86-dea4630afa43",
            "compositeImage": {
                "id": "031743da-6e04-4dfa-ae3b-2626db010b82",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "085ec14f-7252-42f6-8bda-119afffeac2c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7af28e28-949d-4df1-a86e-6898ea4ff554",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "085ec14f-7252-42f6-8bda-119afffeac2c",
                    "LayerId": "719e19d0-15a6-4654-b9a2-2875171b4cab"
                }
            ]
        },
        {
            "id": "d6ab1d80-3d7a-4850-8684-147b9e353b5c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cf630650-02b5-4ef6-8a86-dea4630afa43",
            "compositeImage": {
                "id": "64629e5b-f0ea-43ad-9b4e-c88372ddc558",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6ab1d80-3d7a-4850-8684-147b9e353b5c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c8e4092-1e11-4123-9131-361c6a879685",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6ab1d80-3d7a-4850-8684-147b9e353b5c",
                    "LayerId": "719e19d0-15a6-4654-b9a2-2875171b4cab"
                }
            ]
        },
        {
            "id": "8629b83c-5590-469d-a29e-f200eecc7fe5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cf630650-02b5-4ef6-8a86-dea4630afa43",
            "compositeImage": {
                "id": "eb71ab69-3d83-4167-989e-ee7bba96e10c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8629b83c-5590-469d-a29e-f200eecc7fe5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "41721424-b4c2-49a6-b6f6-e3f9b87237c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8629b83c-5590-469d-a29e-f200eecc7fe5",
                    "LayerId": "719e19d0-15a6-4654-b9a2-2875171b4cab"
                }
            ]
        }
    ],
    "gridX": 16,
    "gridY": 16,
    "height": 24,
    "layers": [
        {
            "id": "719e19d0-15a6-4654-b9a2-2875171b4cab",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cf630650-02b5-4ef6-8a86-dea4630afa43",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 19,
    "xorig": 9,
    "yorig": 23
}