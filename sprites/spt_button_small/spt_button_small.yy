{
    "id": "9d2c1231-9ffc-4fd1-aedd-e6f4dc98be71",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spt_button_small",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 0,
    "bbox_right": 92,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "350e2914-3aa4-4e22-b21a-0dc56cc7bbce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9d2c1231-9ffc-4fd1-aedd-e6f4dc98be71",
            "compositeImage": {
                "id": "41db8f1e-b998-4b17-9298-a6b251f3efd3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "350e2914-3aa4-4e22-b21a-0dc56cc7bbce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d73e22b4-702e-41ff-99f1-d72aa66730d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "350e2914-3aa4-4e22-b21a-0dc56cc7bbce",
                    "LayerId": "d506320a-da97-4424-94bd-ce622422fd4c"
                }
            ]
        }
    ],
    "gridX": 8,
    "gridY": 16,
    "height": 22,
    "layers": [
        {
            "id": "d506320a-da97-4424-94bd-ce622422fd4c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9d2c1231-9ffc-4fd1-aedd-e6f4dc98be71",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 93,
    "xorig": 46,
    "yorig": 11
}