{
    "id": "33485ff6-eb04-4479-9284-822efba104c5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spt_text_box",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 48,
    "bbox_left": 0,
    "bbox_right": 193,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "302d4f87-2812-4f42-a455-10a4e5240206",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "33485ff6-eb04-4479-9284-822efba104c5",
            "compositeImage": {
                "id": "44854b38-ca34-4f37-8efd-deed10b0bd87",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "302d4f87-2812-4f42-a455-10a4e5240206",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a308d7d-5a78-4715-9f38-bebcf4891c76",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "302d4f87-2812-4f42-a455-10a4e5240206",
                    "LayerId": "0ce6fbef-843a-44e1-97dd-d34d3ba6a516"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 49,
    "layers": [
        {
            "id": "0ce6fbef-843a-44e1-97dd-d34d3ba6a516",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "33485ff6-eb04-4479-9284-822efba104c5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 194,
    "xorig": 97,
    "yorig": 24
}