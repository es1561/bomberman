{
    "id": "1c22e8b2-c4e5-4182-83c2-acc868546eca",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spt_fire_side_v",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2e81d537-4579-47a6-bba8-9d216c87fa0f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1c22e8b2-c4e5-4182-83c2-acc868546eca",
            "compositeImage": {
                "id": "bba35a10-8eae-437c-9562-d36f9ce51f76",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e81d537-4579-47a6-bba8-9d216c87fa0f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6752d09a-c126-475c-9c57-8000f5d645f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e81d537-4579-47a6-bba8-9d216c87fa0f",
                    "LayerId": "9de19aa2-1f34-423c-ac5b-ff9852faf588"
                }
            ]
        },
        {
            "id": "a0ad3507-f3a6-42fd-8796-0cb0ea54a6f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1c22e8b2-c4e5-4182-83c2-acc868546eca",
            "compositeImage": {
                "id": "7b2a5df3-a6a6-4443-a102-156f3c6858fe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a0ad3507-f3a6-42fd-8796-0cb0ea54a6f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5d1c66ba-76c3-4a76-82cc-3d021c7212b9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a0ad3507-f3a6-42fd-8796-0cb0ea54a6f2",
                    "LayerId": "9de19aa2-1f34-423c-ac5b-ff9852faf588"
                }
            ]
        },
        {
            "id": "978eba55-336f-4677-a9fd-41cf050536ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1c22e8b2-c4e5-4182-83c2-acc868546eca",
            "compositeImage": {
                "id": "bbd13baf-0ab1-46d7-9a16-033838d961a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "978eba55-336f-4677-a9fd-41cf050536ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fcb004b4-a407-48dc-8110-51037a82e0d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "978eba55-336f-4677-a9fd-41cf050536ec",
                    "LayerId": "9de19aa2-1f34-423c-ac5b-ff9852faf588"
                }
            ]
        },
        {
            "id": "8bf9a34e-77be-48f5-9f2d-1a8a9dcaf597",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1c22e8b2-c4e5-4182-83c2-acc868546eca",
            "compositeImage": {
                "id": "c32773db-75ce-4f66-b6ae-874fdbe7eee2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8bf9a34e-77be-48f5-9f2d-1a8a9dcaf597",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "767d1327-cd42-4653-b538-ba322e46b45d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8bf9a34e-77be-48f5-9f2d-1a8a9dcaf597",
                    "LayerId": "9de19aa2-1f34-423c-ac5b-ff9852faf588"
                }
            ]
        },
        {
            "id": "d157f84d-bdd7-47b9-b85d-db73fb29c770",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1c22e8b2-c4e5-4182-83c2-acc868546eca",
            "compositeImage": {
                "id": "2f9b3672-1503-4fce-8f7b-0e6c0f1c6f7d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d157f84d-bdd7-47b9-b85d-db73fb29c770",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0cfdd758-5987-4d32-b51c-4b717d3c73cc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d157f84d-bdd7-47b9-b85d-db73fb29c770",
                    "LayerId": "9de19aa2-1f34-423c-ac5b-ff9852faf588"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "9de19aa2-1f34-423c-ac5b-ff9852faf588",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1c22e8b2-c4e5-4182-83c2-acc868546eca",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}