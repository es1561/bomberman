{
    "id": "9832001d-57a4-417b-8b39-02fbd594dde4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spt_fire_side_h",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1c4a6a24-d90c-4df9-b9b6-ca1b936d755f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9832001d-57a4-417b-8b39-02fbd594dde4",
            "compositeImage": {
                "id": "5ecd2cff-185d-4a29-9e9a-9383de468ee4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c4a6a24-d90c-4df9-b9b6-ca1b936d755f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "acc0ee80-cea1-4d34-886d-1a6cdc0441fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c4a6a24-d90c-4df9-b9b6-ca1b936d755f",
                    "LayerId": "d323f492-44ea-4ef1-ae48-2d81e3e11602"
                }
            ]
        },
        {
            "id": "79395bb7-2b90-466c-81aa-8f55c3d847bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9832001d-57a4-417b-8b39-02fbd594dde4",
            "compositeImage": {
                "id": "276f81fa-ed49-41cc-ac5b-9add81784a65",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "79395bb7-2b90-466c-81aa-8f55c3d847bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84f3473f-6507-4ef9-b6cf-66d9ed81066b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "79395bb7-2b90-466c-81aa-8f55c3d847bd",
                    "LayerId": "d323f492-44ea-4ef1-ae48-2d81e3e11602"
                }
            ]
        },
        {
            "id": "9cc3a516-3f7a-417b-8f7b-adf62652d06b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9832001d-57a4-417b-8b39-02fbd594dde4",
            "compositeImage": {
                "id": "6cb69b53-20a6-4a9d-ae70-4cc21b5cc415",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9cc3a516-3f7a-417b-8f7b-adf62652d06b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f117a3bf-2c18-4693-bb85-fdf28dcecc02",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9cc3a516-3f7a-417b-8f7b-adf62652d06b",
                    "LayerId": "d323f492-44ea-4ef1-ae48-2d81e3e11602"
                }
            ]
        },
        {
            "id": "9dadbd88-3328-4cdc-8c03-30e45469a613",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9832001d-57a4-417b-8b39-02fbd594dde4",
            "compositeImage": {
                "id": "d3d8e9b8-502d-4cc5-82c3-21347e165526",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9dadbd88-3328-4cdc-8c03-30e45469a613",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "243bafda-1694-44b1-bfaa-1b644374f9c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9dadbd88-3328-4cdc-8c03-30e45469a613",
                    "LayerId": "d323f492-44ea-4ef1-ae48-2d81e3e11602"
                }
            ]
        },
        {
            "id": "25619760-22bf-4faa-bac6-7385ecd02cad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9832001d-57a4-417b-8b39-02fbd594dde4",
            "compositeImage": {
                "id": "5681f509-543b-4201-8d9a-6662695bccf1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25619760-22bf-4faa-bac6-7385ecd02cad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b1c03025-1cda-4df9-a20d-dd4176e22b1e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25619760-22bf-4faa-bac6-7385ecd02cad",
                    "LayerId": "d323f492-44ea-4ef1-ae48-2d81e3e11602"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "d323f492-44ea-4ef1-ae48-2d81e3e11602",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9832001d-57a4-417b-8b39-02fbd594dde4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}