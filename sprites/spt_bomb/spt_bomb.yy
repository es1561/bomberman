{
    "id": "3904518e-0437-4448-82c0-f11bf9801c6d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spt_bomb",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "977744c9-1592-42d3-8bf0-33f7b8b79d25",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3904518e-0437-4448-82c0-f11bf9801c6d",
            "compositeImage": {
                "id": "975fed75-8871-4172-b41a-eca54638572a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "977744c9-1592-42d3-8bf0-33f7b8b79d25",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9115e265-19fa-4d29-ba64-67e45ec21dae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "977744c9-1592-42d3-8bf0-33f7b8b79d25",
                    "LayerId": "de6face4-6805-4253-ac44-7ceb79b29d6a"
                }
            ]
        },
        {
            "id": "8b3a3908-f160-4433-a0a5-c29c9292291d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3904518e-0437-4448-82c0-f11bf9801c6d",
            "compositeImage": {
                "id": "116cdb00-b6b3-4c0f-a4a6-8fc64a02aceb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b3a3908-f160-4433-a0a5-c29c9292291d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "939ef0ba-6bd1-4e2b-b1f9-773f4cad2cac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b3a3908-f160-4433-a0a5-c29c9292291d",
                    "LayerId": "de6face4-6805-4253-ac44-7ceb79b29d6a"
                }
            ]
        },
        {
            "id": "ccd8a768-c1a9-43a8-8042-1070cf56e895",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3904518e-0437-4448-82c0-f11bf9801c6d",
            "compositeImage": {
                "id": "aee0c973-2ba1-419b-82cb-bbd852a662fd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ccd8a768-c1a9-43a8-8042-1070cf56e895",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef0bc2c7-d3c9-4000-b787-318c7280a89c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ccd8a768-c1a9-43a8-8042-1070cf56e895",
                    "LayerId": "de6face4-6805-4253-ac44-7ceb79b29d6a"
                }
            ]
        },
        {
            "id": "57f76475-39af-419a-ac66-f231cc5d3f2f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3904518e-0437-4448-82c0-f11bf9801c6d",
            "compositeImage": {
                "id": "dfe689d7-6b92-4303-97ff-f52cfc16b960",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "57f76475-39af-419a-ac66-f231cc5d3f2f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8debdcce-9d3a-4788-a52a-e84148bd904d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "57f76475-39af-419a-ac66-f231cc5d3f2f",
                    "LayerId": "de6face4-6805-4253-ac44-7ceb79b29d6a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "de6face4-6805-4253-ac44-7ceb79b29d6a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3904518e-0437-4448-82c0-f11bf9801c6d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 4,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}