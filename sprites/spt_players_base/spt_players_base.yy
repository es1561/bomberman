{
    "id": "59cc669b-f3b6-4aa2-996e-058da420e480",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spt_players_base",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 624,
    "bbox_left": 0,
    "bbox_right": 942,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "13140632-871e-4c1b-861f-2295a3110000",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "59cc669b-f3b6-4aa2-996e-058da420e480",
            "compositeImage": {
                "id": "ed5cb355-c413-4839-9c47-7678f25e1a6d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "13140632-871e-4c1b-861f-2295a3110000",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b59dd30-797d-4649-a35a-4aa2fe7f01f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13140632-871e-4c1b-861f-2295a3110000",
                    "LayerId": "fac45903-ea6b-44ce-b201-3e52bcfc926b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 625,
    "layers": [
        {
            "id": "fac45903-ea6b-44ce-b201-3e52bcfc926b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "59cc669b-f3b6-4aa2-996e-058da420e480",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 943,
    "xorig": 0,
    "yorig": 0
}