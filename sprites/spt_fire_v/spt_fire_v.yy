{
    "id": "f2e09490-4034-4b36-a5b5-c02aa643cdd2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spt_fire_v",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2ff7fd89-1fad-4981-a27e-9d634e4818b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f2e09490-4034-4b36-a5b5-c02aa643cdd2",
            "compositeImage": {
                "id": "e1817754-94ed-4809-92a0-9320c980be68",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ff7fd89-1fad-4981-a27e-9d634e4818b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "25617e9c-07b9-4380-a075-42d30284cfe0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ff7fd89-1fad-4981-a27e-9d634e4818b2",
                    "LayerId": "0a9cd0a2-85ab-4b31-89a8-fbfd36e75b79"
                }
            ]
        },
        {
            "id": "ff0e7747-7656-4cae-888d-c8c6cb2b0817",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f2e09490-4034-4b36-a5b5-c02aa643cdd2",
            "compositeImage": {
                "id": "403d3785-1e62-4072-acf5-2ade64705bae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff0e7747-7656-4cae-888d-c8c6cb2b0817",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "61b79b9f-a2ba-47a0-afd9-7b21a0198799",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff0e7747-7656-4cae-888d-c8c6cb2b0817",
                    "LayerId": "0a9cd0a2-85ab-4b31-89a8-fbfd36e75b79"
                }
            ]
        },
        {
            "id": "e08c5de4-3192-40a1-9230-806ee3f9f931",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f2e09490-4034-4b36-a5b5-c02aa643cdd2",
            "compositeImage": {
                "id": "9abcbc8f-c5b9-4a6a-9cf5-00a2857c934c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e08c5de4-3192-40a1-9230-806ee3f9f931",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "77f537f7-73c5-480c-bc59-d9cb8f1c5e8f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e08c5de4-3192-40a1-9230-806ee3f9f931",
                    "LayerId": "0a9cd0a2-85ab-4b31-89a8-fbfd36e75b79"
                }
            ]
        },
        {
            "id": "c3413ea9-8319-4a8b-ba26-a211b7db8d74",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f2e09490-4034-4b36-a5b5-c02aa643cdd2",
            "compositeImage": {
                "id": "86ed4ec6-df15-43bf-9a87-3ede5285dd03",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c3413ea9-8319-4a8b-ba26-a211b7db8d74",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b78f1484-e7f1-47d2-be2f-a93c862e35fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c3413ea9-8319-4a8b-ba26-a211b7db8d74",
                    "LayerId": "0a9cd0a2-85ab-4b31-89a8-fbfd36e75b79"
                }
            ]
        },
        {
            "id": "0b3d7cf2-ac78-49f7-a2f5-eae7e323bbc5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f2e09490-4034-4b36-a5b5-c02aa643cdd2",
            "compositeImage": {
                "id": "393d0866-819e-4f48-bc70-a53847488460",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0b3d7cf2-ac78-49f7-a2f5-eae7e323bbc5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f48bad0-56b9-4299-9c16-8dbe76085a28",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b3d7cf2-ac78-49f7-a2f5-eae7e323bbc5",
                    "LayerId": "0a9cd0a2-85ab-4b31-89a8-fbfd36e75b79"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "0a9cd0a2-85ab-4b31-89a8-fbfd36e75b79",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f2e09490-4034-4b36-a5b5-c02aa643cdd2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}