{
    "id": "40ee2d9c-e792-4d6e-bef9-38eb406fe76d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spt_fire_top_v",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "252ded22-cea5-4095-a1af-529714fe1b95",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "40ee2d9c-e792-4d6e-bef9-38eb406fe76d",
            "compositeImage": {
                "id": "f61b2b60-173b-41f1-9d19-61fb318ece0b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "252ded22-cea5-4095-a1af-529714fe1b95",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f2e03816-dfb9-4a65-a3dd-6c72df837db4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "252ded22-cea5-4095-a1af-529714fe1b95",
                    "LayerId": "b47c5851-2dee-4188-8fca-da12311c28d4"
                }
            ]
        },
        {
            "id": "b83d28ea-39c6-4cb6-ada0-06344f49ff8a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "40ee2d9c-e792-4d6e-bef9-38eb406fe76d",
            "compositeImage": {
                "id": "b2c69551-0fbf-4963-b664-83959ba34e8c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b83d28ea-39c6-4cb6-ada0-06344f49ff8a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0cd5e83f-ee60-49a5-908f-fd2de14997ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b83d28ea-39c6-4cb6-ada0-06344f49ff8a",
                    "LayerId": "b47c5851-2dee-4188-8fca-da12311c28d4"
                }
            ]
        },
        {
            "id": "0a6e9303-f364-4f01-87f6-86143b16da48",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "40ee2d9c-e792-4d6e-bef9-38eb406fe76d",
            "compositeImage": {
                "id": "b2e2ffe9-d417-4295-b343-7e90e57d2467",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a6e9303-f364-4f01-87f6-86143b16da48",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a54bf6df-7201-40cb-b7c3-38aa4501e063",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a6e9303-f364-4f01-87f6-86143b16da48",
                    "LayerId": "b47c5851-2dee-4188-8fca-da12311c28d4"
                }
            ]
        },
        {
            "id": "bcbf7e14-8609-4d13-8a54-7d8d7ae6648d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "40ee2d9c-e792-4d6e-bef9-38eb406fe76d",
            "compositeImage": {
                "id": "7a876948-2c4b-4748-8e9f-168415a63da7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bcbf7e14-8609-4d13-8a54-7d8d7ae6648d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae2352c6-4650-4896-b720-d044e6b8951d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bcbf7e14-8609-4d13-8a54-7d8d7ae6648d",
                    "LayerId": "b47c5851-2dee-4188-8fca-da12311c28d4"
                }
            ]
        },
        {
            "id": "4a98f70c-6b87-4329-a97a-f5739adbb14c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "40ee2d9c-e792-4d6e-bef9-38eb406fe76d",
            "compositeImage": {
                "id": "1a99cfd4-bf6c-44bd-b5b2-2e6576a2c4cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a98f70c-6b87-4329-a97a-f5739adbb14c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c5d8c73-855f-4133-842c-41a088887ba3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a98f70c-6b87-4329-a97a-f5739adbb14c",
                    "LayerId": "b47c5851-2dee-4188-8fca-da12311c28d4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "b47c5851-2dee-4188-8fca-da12311c28d4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "40ee2d9c-e792-4d6e-bef9-38eb406fe76d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}