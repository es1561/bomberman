{
    "id": "938d45fc-a2a7-46bb-9c01-994a9afc857b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spt_fire_mid_v",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d527fcd9-f830-4314-bf23-90dc2f5124d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "938d45fc-a2a7-46bb-9c01-994a9afc857b",
            "compositeImage": {
                "id": "5ee277d6-40eb-4235-a8fb-674e6f59573f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d527fcd9-f830-4314-bf23-90dc2f5124d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "37edae04-aa0e-4bc8-afd2-6afc37f75dbd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d527fcd9-f830-4314-bf23-90dc2f5124d6",
                    "LayerId": "ed07f30a-462e-4e1b-993f-3e0581f2a5b4"
                }
            ]
        },
        {
            "id": "005ce584-6d8f-43b4-b828-d89ef7963471",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "938d45fc-a2a7-46bb-9c01-994a9afc857b",
            "compositeImage": {
                "id": "4aa20075-08e7-4d7f-ac59-aeca9b07fa9a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "005ce584-6d8f-43b4-b828-d89ef7963471",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e9f9bd9-3291-4251-81d7-11bb8f8d44ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "005ce584-6d8f-43b4-b828-d89ef7963471",
                    "LayerId": "ed07f30a-462e-4e1b-993f-3e0581f2a5b4"
                }
            ]
        },
        {
            "id": "ed1b913a-c039-44da-b95d-e277dbedb222",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "938d45fc-a2a7-46bb-9c01-994a9afc857b",
            "compositeImage": {
                "id": "42a5f5d3-1e66-4822-9b90-d6293ea6ee32",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed1b913a-c039-44da-b95d-e277dbedb222",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d493e695-690e-4eb7-bbe0-981e9ea5d661",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed1b913a-c039-44da-b95d-e277dbedb222",
                    "LayerId": "ed07f30a-462e-4e1b-993f-3e0581f2a5b4"
                }
            ]
        },
        {
            "id": "b68f8099-bd46-41e5-930a-508184625369",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "938d45fc-a2a7-46bb-9c01-994a9afc857b",
            "compositeImage": {
                "id": "a01007cf-34e0-41c8-bc45-eb295b6b3672",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b68f8099-bd46-41e5-930a-508184625369",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9414f8d6-2445-460a-9918-4d789e7f4475",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b68f8099-bd46-41e5-930a-508184625369",
                    "LayerId": "ed07f30a-462e-4e1b-993f-3e0581f2a5b4"
                }
            ]
        },
        {
            "id": "136bdfa3-8600-4b47-aad8-2c9c436ff899",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "938d45fc-a2a7-46bb-9c01-994a9afc857b",
            "compositeImage": {
                "id": "1d71b063-d4dd-4938-9328-511d80de5a61",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "136bdfa3-8600-4b47-aad8-2c9c436ff899",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a5fb6787-96d0-4eb7-89b5-2f9b152488de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "136bdfa3-8600-4b47-aad8-2c9c436ff899",
                    "LayerId": "ed07f30a-462e-4e1b-993f-3e0581f2a5b4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "ed07f30a-462e-4e1b-993f-3e0581f2a5b4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "938d45fc-a2a7-46bb-9c01-994a9afc857b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}