{
    "id": "e00c4cd4-3066-49fa-8587-d40b42c75b24",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spt_bomb_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 13,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2e136d74-a000-41ea-bdf5-5ad33bcb1d5b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e00c4cd4-3066-49fa-8587-d40b42c75b24",
            "compositeImage": {
                "id": "00c254e1-b1c6-414f-bf16-7160453bdbf4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e136d74-a000-41ea-bdf5-5ad33bcb1d5b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a7eaf6e-ef96-440a-88ae-cb63ed0d4f21",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e136d74-a000-41ea-bdf5-5ad33bcb1d5b",
                    "LayerId": "b4eb0bf4-5186-45a9-9d44-50c27c4edf5f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "b4eb0bf4-5186-45a9-9d44-50c27c4edf5f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e00c4cd4-3066-49fa-8587-d40b42c75b24",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 14,
    "xorig": 7,
    "yorig": 23
}