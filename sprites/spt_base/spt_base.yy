{
    "id": "9b7de1d0-c165-45c1-be9a-46c821181488",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spt_base",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 734,
    "bbox_left": 0,
    "bbox_right": 1148,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d619d071-1f09-4f8c-ba15-839007f3dc27",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9b7de1d0-c165-45c1-be9a-46c821181488",
            "compositeImage": {
                "id": "024bae60-a0b9-4ad7-ab7a-be08e0d44b01",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d619d071-1f09-4f8c-ba15-839007f3dc27",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "60cd3a7a-51a5-41c5-b244-170fb60b635d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d619d071-1f09-4f8c-ba15-839007f3dc27",
                    "LayerId": "39ad9dec-144e-4a24-af1c-df7944e5914c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 735,
    "layers": [
        {
            "id": "39ad9dec-144e-4a24-af1c-df7944e5914c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9b7de1d0-c165-45c1-be9a-46c821181488",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1149,
    "xorig": 0,
    "yorig": 0
}