{
    "id": "dc7447a2-2084-4983-99f4-69ab4520dfe5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite17",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 255,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "137de142-65d7-4347-b5d7-6cab99577ada",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dc7447a2-2084-4983-99f4-69ab4520dfe5",
            "compositeImage": {
                "id": "ccc462d4-0928-47b4-ae44-d6ec7470d199",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "137de142-65d7-4347-b5d7-6cab99577ada",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b97a0df-adbf-43ea-aae2-94565cd2bb0c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "137de142-65d7-4347-b5d7-6cab99577ada",
                    "LayerId": "6dd291da-72e2-4349-9d67-195ed059a314"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "6dd291da-72e2-4349-9d67-195ed059a314",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dc7447a2-2084-4983-99f4-69ab4520dfe5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 32
}