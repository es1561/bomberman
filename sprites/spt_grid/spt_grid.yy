{
    "id": "32b250ab-6dc3-4a06-b405-c77fd56b9ce8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spt_grid",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a911b020-1479-456e-9f4c-9729281ebdf2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "32b250ab-6dc3-4a06-b405-c77fd56b9ce8",
            "compositeImage": {
                "id": "75748f3d-bfcc-4ba3-bbee-1b51e5af36b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a911b020-1479-456e-9f4c-9729281ebdf2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "38912808-95ce-45a9-9640-15a1a065efbd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a911b020-1479-456e-9f4c-9729281ebdf2",
                    "LayerId": "564bdbb4-8eda-45d0-ab63-11f0c48249cf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "564bdbb4-8eda-45d0-ab63-11f0c48249cf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "32b250ab-6dc3-4a06-b405-c77fd56b9ce8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}