{
    "id": "d7dde2ad-cb8d-429b-9a19-4d6674d3463a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spt_bomb_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 13,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7d28fa8a-cd90-4c0d-ab49-c3e5ef7ace05",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7dde2ad-cb8d-429b-9a19-4d6674d3463a",
            "compositeImage": {
                "id": "49163d10-1c0c-4ad9-8ed1-6f40d575549f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d28fa8a-cd90-4c0d-ab49-c3e5ef7ace05",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce4a289f-a561-48d1-9a67-c6b125ff9d7b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d28fa8a-cd90-4c0d-ab49-c3e5ef7ace05",
                    "LayerId": "fe150426-730a-42a9-9ac6-ea45330f3104"
                }
            ]
        },
        {
            "id": "10d3080b-753f-45d2-900a-342ff1761017",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7dde2ad-cb8d-429b-9a19-4d6674d3463a",
            "compositeImage": {
                "id": "d3b4202b-34fc-4994-a276-7d97016cbbb8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "10d3080b-753f-45d2-900a-342ff1761017",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c8b1c273-57dd-4d0f-a6e7-b5950db897c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "10d3080b-753f-45d2-900a-342ff1761017",
                    "LayerId": "fe150426-730a-42a9-9ac6-ea45330f3104"
                }
            ]
        },
        {
            "id": "96722da1-7eda-423b-b2a6-4f030de05d44",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7dde2ad-cb8d-429b-9a19-4d6674d3463a",
            "compositeImage": {
                "id": "e80ee558-38ff-4f72-b826-7174c5687840",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96722da1-7eda-423b-b2a6-4f030de05d44",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "01ab9648-2ef8-474b-9955-f63e48cdc732",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96722da1-7eda-423b-b2a6-4f030de05d44",
                    "LayerId": "fe150426-730a-42a9-9ac6-ea45330f3104"
                }
            ]
        },
        {
            "id": "002aaa09-f007-4a4d-a65b-893125bfaf52",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7dde2ad-cb8d-429b-9a19-4d6674d3463a",
            "compositeImage": {
                "id": "26a0bc95-fe0f-4594-9f4d-0af864ea404c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "002aaa09-f007-4a4d-a65b-893125bfaf52",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1647a593-9a04-4722-b9a3-8b8840e51c46",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "002aaa09-f007-4a4d-a65b-893125bfaf52",
                    "LayerId": "fe150426-730a-42a9-9ac6-ea45330f3104"
                }
            ]
        },
        {
            "id": "81b79d78-2365-45c9-a2bb-643289a46b3f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7dde2ad-cb8d-429b-9a19-4d6674d3463a",
            "compositeImage": {
                "id": "12da05dc-3bff-4a8c-9ae1-3f96ffc2251d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "81b79d78-2365-45c9-a2bb-643289a46b3f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0f5e71c7-8425-4a43-a571-ed3ad603ecb9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81b79d78-2365-45c9-a2bb-643289a46b3f",
                    "LayerId": "fe150426-730a-42a9-9ac6-ea45330f3104"
                }
            ]
        },
        {
            "id": "a64e26cb-bfe2-4958-aec7-7092e9203efb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7dde2ad-cb8d-429b-9a19-4d6674d3463a",
            "compositeImage": {
                "id": "2949c52b-6b6e-4821-966d-9a2a951d6638",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a64e26cb-bfe2-4958-aec7-7092e9203efb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d3f7486f-4dbd-42ee-9fb3-49442ce951a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a64e26cb-bfe2-4958-aec7-7092e9203efb",
                    "LayerId": "fe150426-730a-42a9-9ac6-ea45330f3104"
                }
            ]
        },
        {
            "id": "81f42627-8443-4ef3-a695-8ae449353598",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7dde2ad-cb8d-429b-9a19-4d6674d3463a",
            "compositeImage": {
                "id": "a3a580bb-13f8-4b52-9c5a-0f416795fb00",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "81f42627-8443-4ef3-a695-8ae449353598",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "064ab9d8-38be-474c-901b-142a2cd21c6c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81f42627-8443-4ef3-a695-8ae449353598",
                    "LayerId": "fe150426-730a-42a9-9ac6-ea45330f3104"
                }
            ]
        },
        {
            "id": "c7eb1464-60c5-45c0-b06b-74a2f92eaa24",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7dde2ad-cb8d-429b-9a19-4d6674d3463a",
            "compositeImage": {
                "id": "097fbd2d-2539-446b-b2b7-59cb6ca0b4d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c7eb1464-60c5-45c0-b06b-74a2f92eaa24",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d37464ea-05ea-4ef1-bb47-e7c2ed742f36",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c7eb1464-60c5-45c0-b06b-74a2f92eaa24",
                    "LayerId": "fe150426-730a-42a9-9ac6-ea45330f3104"
                }
            ]
        }
    ],
    "gridX": 14,
    "gridY": 16,
    "height": 24,
    "layers": [
        {
            "id": "fe150426-730a-42a9-9ac6-ea45330f3104",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d7dde2ad-cb8d-429b-9a19-4d6674d3463a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 14,
    "xorig": 7,
    "yorig": 23
}