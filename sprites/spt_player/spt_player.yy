{
    "id": "f08bbfbc-0b9b-4966-9b1c-44dd89ecc107",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spt_player",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bba8554a-fb9d-4176-a99e-52757ec734b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f08bbfbc-0b9b-4966-9b1c-44dd89ecc107",
            "compositeImage": {
                "id": "d6cc8ec7-060b-4ca6-a8ac-bcd61b034eb4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bba8554a-fb9d-4176-a99e-52757ec734b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a3f72420-dda1-4a3e-bee4-45a5ccaafc45",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bba8554a-fb9d-4176-a99e-52757ec734b7",
                    "LayerId": "cb3cb971-f647-483c-937a-d3be750d9711"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "cb3cb971-f647-483c-937a-d3be750d9711",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f08bbfbc-0b9b-4966-9b1c-44dd89ecc107",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 15
}