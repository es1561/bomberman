{
    "id": "814b4d44-13a6-409f-94e8-05f0317570a9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spt_player_die",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 18,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6f970e1d-a588-483c-b93f-76ff7a7f1f2b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "814b4d44-13a6-409f-94e8-05f0317570a9",
            "compositeImage": {
                "id": "dbacdb56-f1d3-4b67-bf8b-b5b02d6a8967",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f970e1d-a588-483c-b93f-76ff7a7f1f2b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "906f0fdb-3097-4e21-bf9b-8621ec287e36",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f970e1d-a588-483c-b93f-76ff7a7f1f2b",
                    "LayerId": "8141e548-fc53-4b2c-a442-c6cacdab1df8"
                }
            ]
        },
        {
            "id": "1fd63019-5908-42fe-b95e-fa42342719b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "814b4d44-13a6-409f-94e8-05f0317570a9",
            "compositeImage": {
                "id": "d82a81ab-048e-4377-a678-b3d115d156d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1fd63019-5908-42fe-b95e-fa42342719b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0fd666ac-68b9-41ad-885f-7e1a1ae298b9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1fd63019-5908-42fe-b95e-fa42342719b5",
                    "LayerId": "8141e548-fc53-4b2c-a442-c6cacdab1df8"
                }
            ]
        },
        {
            "id": "1ad2c2e3-be53-4579-8538-de2715180ab4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "814b4d44-13a6-409f-94e8-05f0317570a9",
            "compositeImage": {
                "id": "e1f60df3-dd58-4e35-9693-d8bf19cead2d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ad2c2e3-be53-4579-8538-de2715180ab4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f6baf100-025d-455c-807e-000964ed6a7b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ad2c2e3-be53-4579-8538-de2715180ab4",
                    "LayerId": "8141e548-fc53-4b2c-a442-c6cacdab1df8"
                }
            ]
        },
        {
            "id": "615cdcee-03d9-4330-ab75-e46a69e7b06a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "814b4d44-13a6-409f-94e8-05f0317570a9",
            "compositeImage": {
                "id": "3870de4e-edfc-4263-be18-85183c479f13",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "615cdcee-03d9-4330-ab75-e46a69e7b06a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ee32918-92e6-4aa4-adee-66bcbaef52e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "615cdcee-03d9-4330-ab75-e46a69e7b06a",
                    "LayerId": "8141e548-fc53-4b2c-a442-c6cacdab1df8"
                }
            ]
        },
        {
            "id": "274c3d26-8bd7-4135-9a21-cc3b51d709a2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "814b4d44-13a6-409f-94e8-05f0317570a9",
            "compositeImage": {
                "id": "ac0931ec-ab6b-47a5-8e60-d67cf875bedd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "274c3d26-8bd7-4135-9a21-cc3b51d709a2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc7236dd-48b0-4e40-86f8-a9c7e344796d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "274c3d26-8bd7-4135-9a21-cc3b51d709a2",
                    "LayerId": "8141e548-fc53-4b2c-a442-c6cacdab1df8"
                }
            ]
        }
    ],
    "gridX": 16,
    "gridY": 16,
    "height": 24,
    "layers": [
        {
            "id": "8141e548-fc53-4b2c-a442-c6cacdab1df8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "814b4d44-13a6-409f-94e8-05f0317570a9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 19,
    "xorig": 9,
    "yorig": 23
}