{
    "id": "bc0bc9e6-741c-49f3-aad9-df775bc4be9e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spt_tile",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 17,
    "bbox_right": 79,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "594e2451-6a2b-4ced-b673-f53485a01e92",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bc0bc9e6-741c-49f3-aad9-df775bc4be9e",
            "compositeImage": {
                "id": "1ea0f620-a77e-40fa-bb2b-9ff3826c749e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "594e2451-6a2b-4ced-b673-f53485a01e92",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4cd82e30-9e00-400d-b0f2-dc8483907dee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "594e2451-6a2b-4ced-b673-f53485a01e92",
                    "LayerId": "8b479b9c-e060-4a03-ab28-b9a228846992"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "8b479b9c-e060-4a03-ab28-b9a228846992",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bc0bc9e6-741c-49f3-aad9-df775bc4be9e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 80,
    "xorig": 0,
    "yorig": 0
}