{
    "id": "5eacc045-b4e2-45ab-ba0b-5738de39be7c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spt_bomb_base",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ce83613b-6b89-4620-96be-3569a92bc64f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5eacc045-b4e2-45ab-ba0b-5738de39be7c",
            "compositeImage": {
                "id": "bb74e6e5-5117-4a6c-9788-be9cb619ac9c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ce83613b-6b89-4620-96be-3569a92bc64f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "834e2f64-ab81-490c-9046-99244c990d71",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ce83613b-6b89-4620-96be-3569a92bc64f",
                    "LayerId": "136e02a1-9d9a-4f9f-aa09-f74184bd544a"
                }
            ]
        },
        {
            "id": "7c10eeaa-ee3b-46e3-89b7-ae676b622f48",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5eacc045-b4e2-45ab-ba0b-5738de39be7c",
            "compositeImage": {
                "id": "a9e8decb-2614-4840-9902-e2a4f6cd9a73",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c10eeaa-ee3b-46e3-89b7-ae676b622f48",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1382c095-96fb-4129-9c4c-7758ef7b048a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c10eeaa-ee3b-46e3-89b7-ae676b622f48",
                    "LayerId": "136e02a1-9d9a-4f9f-aa09-f74184bd544a"
                }
            ]
        },
        {
            "id": "ca8823a6-6961-4866-aa09-7ab0a60d7495",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5eacc045-b4e2-45ab-ba0b-5738de39be7c",
            "compositeImage": {
                "id": "4704e3d7-694e-4936-86a6-5f0a8bd88c8b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca8823a6-6961-4866-aa09-7ab0a60d7495",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4deb5f5a-67fc-41c2-9edb-b1d239fa6b44",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca8823a6-6961-4866-aa09-7ab0a60d7495",
                    "LayerId": "136e02a1-9d9a-4f9f-aa09-f74184bd544a"
                }
            ]
        },
        {
            "id": "675ece84-3a45-4b61-b403-c3149590f175",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5eacc045-b4e2-45ab-ba0b-5738de39be7c",
            "compositeImage": {
                "id": "03980098-09e3-4361-b634-082d0891cc0b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "675ece84-3a45-4b61-b403-c3149590f175",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "717e3b36-5660-431a-bf18-48de42730628",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "675ece84-3a45-4b61-b403-c3149590f175",
                    "LayerId": "136e02a1-9d9a-4f9f-aa09-f74184bd544a"
                }
            ]
        },
        {
            "id": "fd0cf0c6-89fa-4188-97a6-38efc4650821",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5eacc045-b4e2-45ab-ba0b-5738de39be7c",
            "compositeImage": {
                "id": "49213e47-1067-4dfc-8e70-92c40600f473",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd0cf0c6-89fa-4188-97a6-38efc4650821",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9523d74a-4b73-44ab-87f7-cd3ed3004247",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd0cf0c6-89fa-4188-97a6-38efc4650821",
                    "LayerId": "136e02a1-9d9a-4f9f-aa09-f74184bd544a"
                }
            ]
        },
        {
            "id": "8cb74946-a73e-45af-b01e-a96db8fdb072",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5eacc045-b4e2-45ab-ba0b-5738de39be7c",
            "compositeImage": {
                "id": "b83826f5-ea6b-4e1f-95e1-99abe6d0487a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8cb74946-a73e-45af-b01e-a96db8fdb072",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d5c227a-f7c9-4b06-838c-6524729fcbb6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8cb74946-a73e-45af-b01e-a96db8fdb072",
                    "LayerId": "136e02a1-9d9a-4f9f-aa09-f74184bd544a"
                }
            ]
        },
        {
            "id": "ee7eeae3-24f4-4285-9cd5-0120db0a2fc3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5eacc045-b4e2-45ab-ba0b-5738de39be7c",
            "compositeImage": {
                "id": "2d079385-46a1-4864-be1c-c039470107d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee7eeae3-24f4-4285-9cd5-0120db0a2fc3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c84991d-b350-44af-996a-38b3e47ec900",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee7eeae3-24f4-4285-9cd5-0120db0a2fc3",
                    "LayerId": "136e02a1-9d9a-4f9f-aa09-f74184bd544a"
                }
            ]
        },
        {
            "id": "4d13498f-493f-4f41-af66-9d5b2bbebbeb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5eacc045-b4e2-45ab-ba0b-5738de39be7c",
            "compositeImage": {
                "id": "585403fd-cfa3-42ab-adef-5c69ee7e96e0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d13498f-493f-4f41-af66-9d5b2bbebbeb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a5cfe04a-e191-4643-8927-71f4999b862a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d13498f-493f-4f41-af66-9d5b2bbebbeb",
                    "LayerId": "136e02a1-9d9a-4f9f-aa09-f74184bd544a"
                }
            ]
        },
        {
            "id": "25ad1e2b-e126-494c-9d9e-0a2b1a346b49",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5eacc045-b4e2-45ab-ba0b-5738de39be7c",
            "compositeImage": {
                "id": "9352ec6b-7def-4a47-aede-1120329325cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25ad1e2b-e126-494c-9d9e-0a2b1a346b49",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "de86a743-622b-423a-badd-70d525170ce9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25ad1e2b-e126-494c-9d9e-0a2b1a346b49",
                    "LayerId": "136e02a1-9d9a-4f9f-aa09-f74184bd544a"
                }
            ]
        },
        {
            "id": "ace3ae62-afa1-4980-a18e-8915ca3e1211",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5eacc045-b4e2-45ab-ba0b-5738de39be7c",
            "compositeImage": {
                "id": "bff707eb-d577-49c2-8d72-6f58a448c395",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ace3ae62-afa1-4980-a18e-8915ca3e1211",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b2a63612-3a0b-45c8-9023-89798660b8b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ace3ae62-afa1-4980-a18e-8915ca3e1211",
                    "LayerId": "136e02a1-9d9a-4f9f-aa09-f74184bd544a"
                }
            ]
        },
        {
            "id": "71c67905-5118-42d6-b027-ad1d721e68b9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5eacc045-b4e2-45ab-ba0b-5738de39be7c",
            "compositeImage": {
                "id": "4e297a8b-7abe-4f2f-823e-eca4e8c75f0a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "71c67905-5118-42d6-b027-ad1d721e68b9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cbbf3359-b20a-4a90-8238-d29079dcd19f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "71c67905-5118-42d6-b027-ad1d721e68b9",
                    "LayerId": "136e02a1-9d9a-4f9f-aa09-f74184bd544a"
                }
            ]
        },
        {
            "id": "0ad16eb6-e22f-4784-9e4c-39ce74b2649f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5eacc045-b4e2-45ab-ba0b-5738de39be7c",
            "compositeImage": {
                "id": "5c2dc0f5-e0a8-46d4-a418-a879c5e58ced",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ad16eb6-e22f-4784-9e4c-39ce74b2649f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ca9ef8d-b835-44ff-bb97-ff58c7d0377d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ad16eb6-e22f-4784-9e4c-39ce74b2649f",
                    "LayerId": "136e02a1-9d9a-4f9f-aa09-f74184bd544a"
                }
            ]
        },
        {
            "id": "f5251f06-b165-4616-b51c-b65a8bfba7d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5eacc045-b4e2-45ab-ba0b-5738de39be7c",
            "compositeImage": {
                "id": "bb71865c-554a-4aae-87f4-121faea9b6c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f5251f06-b165-4616-b51c-b65a8bfba7d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "43c5bcb2-a032-46dd-b79d-86150bf9b82b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f5251f06-b165-4616-b51c-b65a8bfba7d9",
                    "LayerId": "136e02a1-9d9a-4f9f-aa09-f74184bd544a"
                }
            ]
        },
        {
            "id": "16446d0a-ce58-42c1-8a83-ab563cf78cab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5eacc045-b4e2-45ab-ba0b-5738de39be7c",
            "compositeImage": {
                "id": "57be3332-531e-4146-90e8-e647b88422bb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "16446d0a-ce58-42c1-8a83-ab563cf78cab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f24fd8d7-9e51-47b3-9ed4-6ff6d71b4b7d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "16446d0a-ce58-42c1-8a83-ab563cf78cab",
                    "LayerId": "136e02a1-9d9a-4f9f-aa09-f74184bd544a"
                }
            ]
        },
        {
            "id": "a7b55224-0474-45b0-85ed-a1bfbdde905c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5eacc045-b4e2-45ab-ba0b-5738de39be7c",
            "compositeImage": {
                "id": "a4197c90-0808-43e9-8e16-30d9badaee8b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a7b55224-0474-45b0-85ed-a1bfbdde905c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f51bafa-ea94-45b7-8529-53cc00ac624e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a7b55224-0474-45b0-85ed-a1bfbdde905c",
                    "LayerId": "136e02a1-9d9a-4f9f-aa09-f74184bd544a"
                }
            ]
        },
        {
            "id": "760d780b-bd29-4e85-89bf-85a34b5b49ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5eacc045-b4e2-45ab-ba0b-5738de39be7c",
            "compositeImage": {
                "id": "bdea9768-7224-4211-a4f6-aea080b5aa70",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "760d780b-bd29-4e85-89bf-85a34b5b49ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ebb8ff5a-2850-4641-b0d7-5e498706aa2c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "760d780b-bd29-4e85-89bf-85a34b5b49ed",
                    "LayerId": "136e02a1-9d9a-4f9f-aa09-f74184bd544a"
                }
            ]
        },
        {
            "id": "40ac7819-f998-45fc-9620-45157c33e82b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5eacc045-b4e2-45ab-ba0b-5738de39be7c",
            "compositeImage": {
                "id": "83cdb702-3bbf-4d64-906f-37fbe2ef5506",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "40ac7819-f998-45fc-9620-45157c33e82b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e72818b3-145d-44be-81ad-d9333abc88b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "40ac7819-f998-45fc-9620-45157c33e82b",
                    "LayerId": "136e02a1-9d9a-4f9f-aa09-f74184bd544a"
                }
            ]
        },
        {
            "id": "880fc4f9-dd37-4cd1-81f9-e90a48f5bdf0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5eacc045-b4e2-45ab-ba0b-5738de39be7c",
            "compositeImage": {
                "id": "472e0ca0-6e5f-4ab8-87ac-53ba5e773c0d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "880fc4f9-dd37-4cd1-81f9-e90a48f5bdf0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a19c3fb2-b1a9-4425-abc8-834329521c65",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "880fc4f9-dd37-4cd1-81f9-e90a48f5bdf0",
                    "LayerId": "136e02a1-9d9a-4f9f-aa09-f74184bd544a"
                }
            ]
        },
        {
            "id": "34e0f320-2f6c-40e1-952d-1c6a7b450105",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5eacc045-b4e2-45ab-ba0b-5738de39be7c",
            "compositeImage": {
                "id": "9666aa2b-7dc2-4789-9620-27332b449f62",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "34e0f320-2f6c-40e1-952d-1c6a7b450105",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "27452fbf-fa59-487b-a150-8280385c675e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "34e0f320-2f6c-40e1-952d-1c6a7b450105",
                    "LayerId": "136e02a1-9d9a-4f9f-aa09-f74184bd544a"
                }
            ]
        },
        {
            "id": "8ca9635c-fab2-45e5-b730-3c2c36194dba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5eacc045-b4e2-45ab-ba0b-5738de39be7c",
            "compositeImage": {
                "id": "58c1015c-2e23-4f78-94fd-7fd59cd8d672",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ca9635c-fab2-45e5-b730-3c2c36194dba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d0f589a-fd20-495b-b78b-7c4c9aedbe32",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ca9635c-fab2-45e5-b730-3c2c36194dba",
                    "LayerId": "136e02a1-9d9a-4f9f-aa09-f74184bd544a"
                }
            ]
        },
        {
            "id": "b22b1713-666a-450f-b37f-b0a95c2f84f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5eacc045-b4e2-45ab-ba0b-5738de39be7c",
            "compositeImage": {
                "id": "59a87212-496a-43bd-a439-26aee15dd0e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b22b1713-666a-450f-b37f-b0a95c2f84f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c199100b-8d62-4fbe-ba61-08aa6273c68b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b22b1713-666a-450f-b37f-b0a95c2f84f1",
                    "LayerId": "136e02a1-9d9a-4f9f-aa09-f74184bd544a"
                }
            ]
        },
        {
            "id": "41f94837-f9b5-4071-aa4b-40998eec6a38",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5eacc045-b4e2-45ab-ba0b-5738de39be7c",
            "compositeImage": {
                "id": "1227ea3f-5a35-4e0f-980f-cbb0a2359b4a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "41f94837-f9b5-4071-aa4b-40998eec6a38",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0101fc9b-51a8-4f70-b0e9-7b2e28830952",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "41f94837-f9b5-4071-aa4b-40998eec6a38",
                    "LayerId": "136e02a1-9d9a-4f9f-aa09-f74184bd544a"
                }
            ]
        },
        {
            "id": "41258f4c-fdae-4144-8545-47fe939302d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5eacc045-b4e2-45ab-ba0b-5738de39be7c",
            "compositeImage": {
                "id": "ac46f0f4-9726-4943-a722-aa0267ca591f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "41258f4c-fdae-4144-8545-47fe939302d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a534262-ad8f-4cca-b946-b4f655fae57a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "41258f4c-fdae-4144-8545-47fe939302d7",
                    "LayerId": "136e02a1-9d9a-4f9f-aa09-f74184bd544a"
                }
            ]
        },
        {
            "id": "f28b9513-8d0a-4a20-a4b8-8d68e3635ee5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5eacc045-b4e2-45ab-ba0b-5738de39be7c",
            "compositeImage": {
                "id": "2f3a0966-ca8e-4152-8422-3725358ed521",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f28b9513-8d0a-4a20-a4b8-8d68e3635ee5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b4000f1d-ea53-43dc-af72-e9f5b5398f6e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f28b9513-8d0a-4a20-a4b8-8d68e3635ee5",
                    "LayerId": "136e02a1-9d9a-4f9f-aa09-f74184bd544a"
                }
            ]
        },
        {
            "id": "ed33ab91-24ea-432d-bab7-6c384cd8cc69",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5eacc045-b4e2-45ab-ba0b-5738de39be7c",
            "compositeImage": {
                "id": "64ee476e-52b7-48e7-b99b-a50f8bb9a064",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed33ab91-24ea-432d-bab7-6c384cd8cc69",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf858432-ebb7-4749-8265-4299852a5a15",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed33ab91-24ea-432d-bab7-6c384cd8cc69",
                    "LayerId": "136e02a1-9d9a-4f9f-aa09-f74184bd544a"
                }
            ]
        },
        {
            "id": "d88dc53d-4a1e-4104-af78-74a67edbed86",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5eacc045-b4e2-45ab-ba0b-5738de39be7c",
            "compositeImage": {
                "id": "9ce781c6-4f4a-4142-824b-0e7f9c7ee6e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d88dc53d-4a1e-4104-af78-74a67edbed86",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5619e527-43e1-421a-8bbc-a949d2ed1b5d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d88dc53d-4a1e-4104-af78-74a67edbed86",
                    "LayerId": "136e02a1-9d9a-4f9f-aa09-f74184bd544a"
                }
            ]
        },
        {
            "id": "b1f41658-7fe5-49ab-b147-b12477a7cc4a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5eacc045-b4e2-45ab-ba0b-5738de39be7c",
            "compositeImage": {
                "id": "f3065896-c064-4f80-8c57-4725866f151a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b1f41658-7fe5-49ab-b147-b12477a7cc4a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c4fecdb3-e017-4007-bc7a-680c88f02918",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b1f41658-7fe5-49ab-b147-b12477a7cc4a",
                    "LayerId": "136e02a1-9d9a-4f9f-aa09-f74184bd544a"
                }
            ]
        },
        {
            "id": "2d49ba6d-a81e-4351-9b0c-0145917622f6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5eacc045-b4e2-45ab-ba0b-5738de39be7c",
            "compositeImage": {
                "id": "cca90794-331f-4e85-ab85-e6d69b1483d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d49ba6d-a81e-4351-9b0c-0145917622f6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f320705d-6379-4703-a3b5-9f76c79837b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d49ba6d-a81e-4351-9b0c-0145917622f6",
                    "LayerId": "136e02a1-9d9a-4f9f-aa09-f74184bd544a"
                }
            ]
        },
        {
            "id": "0db43ead-4ec5-4585-a36b-532712bdca96",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5eacc045-b4e2-45ab-ba0b-5738de39be7c",
            "compositeImage": {
                "id": "b073b10b-47e3-4688-a0d8-8df0c3e57a19",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0db43ead-4ec5-4585-a36b-532712bdca96",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c7b13043-e448-4113-a43c-bdbc7ee675d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0db43ead-4ec5-4585-a36b-532712bdca96",
                    "LayerId": "136e02a1-9d9a-4f9f-aa09-f74184bd544a"
                }
            ]
        },
        {
            "id": "8713055b-ff2e-4b1c-841f-3f77742dded1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5eacc045-b4e2-45ab-ba0b-5738de39be7c",
            "compositeImage": {
                "id": "0921de2c-c09c-4daa-a4b5-17ac94631997",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8713055b-ff2e-4b1c-841f-3f77742dded1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a71402e-7aae-4a98-81f0-725c97e72378",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8713055b-ff2e-4b1c-841f-3f77742dded1",
                    "LayerId": "136e02a1-9d9a-4f9f-aa09-f74184bd544a"
                }
            ]
        },
        {
            "id": "837e5b3c-8038-4989-8425-fb965f984b52",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5eacc045-b4e2-45ab-ba0b-5738de39be7c",
            "compositeImage": {
                "id": "35331307-c443-44ac-a430-1603ac43282e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "837e5b3c-8038-4989-8425-fb965f984b52",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "976a96e5-5d9f-4ac7-ade9-6d7699e06468",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "837e5b3c-8038-4989-8425-fb965f984b52",
                    "LayerId": "136e02a1-9d9a-4f9f-aa09-f74184bd544a"
                }
            ]
        },
        {
            "id": "9f5c72bf-dd0e-4431-8ec2-18b4fabbab57",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5eacc045-b4e2-45ab-ba0b-5738de39be7c",
            "compositeImage": {
                "id": "5c925521-dbd8-49f7-9ce6-287224aaaed2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f5c72bf-dd0e-4431-8ec2-18b4fabbab57",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "211a1172-6bfb-49ba-9bc4-f93538942679",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f5c72bf-dd0e-4431-8ec2-18b4fabbab57",
                    "LayerId": "136e02a1-9d9a-4f9f-aa09-f74184bd544a"
                }
            ]
        },
        {
            "id": "27172a81-e5fb-4264-8cdb-588cf0617294",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5eacc045-b4e2-45ab-ba0b-5738de39be7c",
            "compositeImage": {
                "id": "af9e4d17-5684-48e5-ae48-44f2bdc1a2b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "27172a81-e5fb-4264-8cdb-588cf0617294",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc5616b5-0a04-4b81-9feb-47d4823e78c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "27172a81-e5fb-4264-8cdb-588cf0617294",
                    "LayerId": "136e02a1-9d9a-4f9f-aa09-f74184bd544a"
                }
            ]
        },
        {
            "id": "20d8244e-96a7-4255-8813-4f940ecbbbef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5eacc045-b4e2-45ab-ba0b-5738de39be7c",
            "compositeImage": {
                "id": "04e86963-45d3-4443-9b10-facb7f5dbcb4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "20d8244e-96a7-4255-8813-4f940ecbbbef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa127e9c-7220-49d4-9133-820cc05ed360",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "20d8244e-96a7-4255-8813-4f940ecbbbef",
                    "LayerId": "136e02a1-9d9a-4f9f-aa09-f74184bd544a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "136e02a1-9d9a-4f9f-aa09-f74184bd544a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5eacc045-b4e2-45ab-ba0b-5738de39be7c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}