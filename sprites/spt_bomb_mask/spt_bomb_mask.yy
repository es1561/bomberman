{
    "id": "2b63b3a5-0fec-47e4-ad68-3ce682dd9b01",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spt_bomb_mask",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 1,
    "bbox_right": 12,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ce35940a-e8d8-4109-ac85-27793ca42a3b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2b63b3a5-0fec-47e4-ad68-3ce682dd9b01",
            "compositeImage": {
                "id": "3e4b4d70-3176-436b-9d2d-54ccf4eb6762",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ce35940a-e8d8-4109-ac85-27793ca42a3b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3764ccdd-fb75-41b6-9503-d8f3733bc711",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ce35940a-e8d8-4109-ac85-27793ca42a3b",
                    "LayerId": "367aab5a-583f-435c-8f43-d2617d12134f"
                },
                {
                    "id": "986762cf-b6a9-43ca-b979-37e2a0e672a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ce35940a-e8d8-4109-ac85-27793ca42a3b",
                    "LayerId": "a08e30c1-0bdf-41c2-9c6d-86045320af24"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "a08e30c1-0bdf-41c2-9c6d-86045320af24",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2b63b3a5-0fec-47e4-ad68-3ce682dd9b01",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "367aab5a-583f-435c-8f43-d2617d12134f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2b63b3a5-0fec-47e4-ad68-3ce682dd9b01",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": false
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 14,
    "xorig": 7,
    "yorig": 23
}