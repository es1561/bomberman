{
    "id": "fbda1549-98a9-493c-ac31-b1724fc6b2ee",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spt_fire_mid_h",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "92ac9eef-6dfe-4fb9-98ff-31d31feae251",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fbda1549-98a9-493c-ac31-b1724fc6b2ee",
            "compositeImage": {
                "id": "5820815c-bc3a-41ab-ba3b-b43a07596f62",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "92ac9eef-6dfe-4fb9-98ff-31d31feae251",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "939d98a1-1e97-4ae1-ad4f-6857d19fdfd9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "92ac9eef-6dfe-4fb9-98ff-31d31feae251",
                    "LayerId": "8a8be88d-8338-44b3-9ffc-2c7ac48b6d41"
                }
            ]
        },
        {
            "id": "60a97f60-0134-49c5-865e-4b67d5ba4a27",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fbda1549-98a9-493c-ac31-b1724fc6b2ee",
            "compositeImage": {
                "id": "1d805eae-9d46-47c2-a373-b891002ab26d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "60a97f60-0134-49c5-865e-4b67d5ba4a27",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa609ef2-58e7-41d2-9458-95834af4e963",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "60a97f60-0134-49c5-865e-4b67d5ba4a27",
                    "LayerId": "8a8be88d-8338-44b3-9ffc-2c7ac48b6d41"
                }
            ]
        },
        {
            "id": "fda723c1-a87c-4afd-8da0-4c42f53c1a9a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fbda1549-98a9-493c-ac31-b1724fc6b2ee",
            "compositeImage": {
                "id": "d167f03a-5dbc-407a-9266-85920f06d365",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fda723c1-a87c-4afd-8da0-4c42f53c1a9a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc5e6243-a7a4-427b-917c-91f5d5157de5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fda723c1-a87c-4afd-8da0-4c42f53c1a9a",
                    "LayerId": "8a8be88d-8338-44b3-9ffc-2c7ac48b6d41"
                }
            ]
        },
        {
            "id": "adadad54-f595-4ca5-a037-876687b7044b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fbda1549-98a9-493c-ac31-b1724fc6b2ee",
            "compositeImage": {
                "id": "e1c84734-5fa5-4dfa-a9a1-5f5487a2adfb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "adadad54-f595-4ca5-a037-876687b7044b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d569d659-edc8-4490-8c0b-4a088b6b03a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "adadad54-f595-4ca5-a037-876687b7044b",
                    "LayerId": "8a8be88d-8338-44b3-9ffc-2c7ac48b6d41"
                }
            ]
        },
        {
            "id": "0f3d2955-6e09-4f73-a07c-cdcd89efa178",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fbda1549-98a9-493c-ac31-b1724fc6b2ee",
            "compositeImage": {
                "id": "42846864-67d0-42c8-99c5-2005f19c7dc6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f3d2955-6e09-4f73-a07c-cdcd89efa178",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "034fe285-e8cc-457c-b91b-2537e3b82a67",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f3d2955-6e09-4f73-a07c-cdcd89efa178",
                    "LayerId": "8a8be88d-8338-44b3-9ffc-2c7ac48b6d41"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "8a8be88d-8338-44b3-9ffc-2c7ac48b6d41",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fbda1549-98a9-493c-ac31-b1724fc6b2ee",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}