{
    "id": "de9fb237-d344-4277-9b75-0ae2fd23ab19",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spt_block",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b4aa7254-e29d-44d1-aea9-186bbfd4a3cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "de9fb237-d344-4277-9b75-0ae2fd23ab19",
            "compositeImage": {
                "id": "f372a189-c204-43ce-b706-929818df5000",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4aa7254-e29d-44d1-aea9-186bbfd4a3cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a62b4190-998b-4641-9a4f-397de89a6e55",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4aa7254-e29d-44d1-aea9-186bbfd4a3cf",
                    "LayerId": "1f414bde-d975-4413-9866-2d029199fd75"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 15,
    "layers": [
        {
            "id": "1f414bde-d975-4413-9866-2d029199fd75",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "de9fb237-d344-4277-9b75-0ae2fd23ab19",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}