{
    "id": "03e9c7cf-85e9-4a40-bd3c-00b6e5644ca7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spt_block_die",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9ade8890-fd8d-4f41-910e-7e0ed2c94c0c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03e9c7cf-85e9-4a40-bd3c-00b6e5644ca7",
            "compositeImage": {
                "id": "dbbb0694-9366-4aed-a5ac-7b50d353b713",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ade8890-fd8d-4f41-910e-7e0ed2c94c0c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5dd9403d-a5f5-4a89-b769-15f336bb8a94",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ade8890-fd8d-4f41-910e-7e0ed2c94c0c",
                    "LayerId": "8c30e60f-e8ba-4d9c-975e-fe1e027c0b74"
                }
            ]
        },
        {
            "id": "c0082b2d-89d2-4572-8885-a5e67dde666b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03e9c7cf-85e9-4a40-bd3c-00b6e5644ca7",
            "compositeImage": {
                "id": "dcc99acd-9f37-451b-acb9-5cacb9f27636",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c0082b2d-89d2-4572-8885-a5e67dde666b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6194e2fd-e20e-4111-800e-d1766fc089b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c0082b2d-89d2-4572-8885-a5e67dde666b",
                    "LayerId": "8c30e60f-e8ba-4d9c-975e-fe1e027c0b74"
                }
            ]
        },
        {
            "id": "708654b4-b72f-4921-bce5-48fcfc180d43",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03e9c7cf-85e9-4a40-bd3c-00b6e5644ca7",
            "compositeImage": {
                "id": "c59d1650-2f04-434c-af5c-74a528f2f72d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "708654b4-b72f-4921-bce5-48fcfc180d43",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fe173d6c-3fc9-400b-aab6-a129a8ee291d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "708654b4-b72f-4921-bce5-48fcfc180d43",
                    "LayerId": "8c30e60f-e8ba-4d9c-975e-fe1e027c0b74"
                }
            ]
        },
        {
            "id": "e69ed891-3d7a-45ec-b6b8-fdf1b6ad4403",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03e9c7cf-85e9-4a40-bd3c-00b6e5644ca7",
            "compositeImage": {
                "id": "863c51a8-7994-4e03-bbb3-26c3d56763de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e69ed891-3d7a-45ec-b6b8-fdf1b6ad4403",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "64944670-772b-4781-aa30-3b8a3b7275d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e69ed891-3d7a-45ec-b6b8-fdf1b6ad4403",
                    "LayerId": "8c30e60f-e8ba-4d9c-975e-fe1e027c0b74"
                }
            ]
        },
        {
            "id": "2e2a247f-51e7-4f7e-8220-256319265fce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03e9c7cf-85e9-4a40-bd3c-00b6e5644ca7",
            "compositeImage": {
                "id": "fe2710a9-76f9-4d8d-8f94-448647f9a4ba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e2a247f-51e7-4f7e-8220-256319265fce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a944f20d-3f5f-4949-9b4d-ca146ff207d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e2a247f-51e7-4f7e-8220-256319265fce",
                    "LayerId": "8c30e60f-e8ba-4d9c-975e-fe1e027c0b74"
                }
            ]
        },
        {
            "id": "30959d96-d943-4d9b-bf97-738d1b227903",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03e9c7cf-85e9-4a40-bd3c-00b6e5644ca7",
            "compositeImage": {
                "id": "dc426834-ee53-4ecc-8ea5-4ad4bff271ed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "30959d96-d943-4d9b-bf97-738d1b227903",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "70c4bdb7-b2e9-4de8-abae-7fb6e929d8ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "30959d96-d943-4d9b-bf97-738d1b227903",
                    "LayerId": "8c30e60f-e8ba-4d9c-975e-fe1e027c0b74"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "8c30e60f-e8ba-4d9c-975e-fe1e027c0b74",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "03e9c7cf-85e9-4a40-bd3c-00b6e5644ca7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 9,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}