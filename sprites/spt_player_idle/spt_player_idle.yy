{
    "id": "081b267b-c1c0-41cd-adca-ef9f8faf1609",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spt_player_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 3,
    "bbox_right": 16,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4a790b4c-7a9b-41be-9483-e04f99a5b7ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "081b267b-c1c0-41cd-adca-ef9f8faf1609",
            "compositeImage": {
                "id": "4d7e71ab-ebeb-4afd-8d40-c1f68eb78539",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a790b4c-7a9b-41be-9483-e04f99a5b7ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "26da0d14-1b97-47bf-8d3a-64df3cb3ebd2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a790b4c-7a9b-41be-9483-e04f99a5b7ad",
                    "LayerId": "d7fda3a2-696a-4c66-af47-8418d1be961c"
                }
            ]
        }
    ],
    "gridX": 16,
    "gridY": 16,
    "height": 24,
    "layers": [
        {
            "id": "d7fda3a2-696a-4c66-af47-8418d1be961c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "081b267b-c1c0-41cd-adca-ef9f8faf1609",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 19,
    "xorig": 9,
    "yorig": 23
}