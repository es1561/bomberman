{
    "id": "6b7e8428-d0db-4fc6-acb6-5c80ca4140c3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spt_bomb_side",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 0,
    "bbox_right": 16,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "eb5447bc-bb51-4d0e-9485-d1c9094d632e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b7e8428-d0db-4fc6-acb6-5c80ca4140c3",
            "compositeImage": {
                "id": "779c364f-c9a7-4894-b944-5ba86dac455c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb5447bc-bb51-4d0e-9485-d1c9094d632e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "23b564dc-5b6c-40c8-8348-4b8fae6f5990",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb5447bc-bb51-4d0e-9485-d1c9094d632e",
                    "LayerId": "94ca47b4-d256-4c5c-8a74-7e81cd7e181a"
                }
            ]
        },
        {
            "id": "bd4036ca-3586-4053-8e0c-ad739c64e8af",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b7e8428-d0db-4fc6-acb6-5c80ca4140c3",
            "compositeImage": {
                "id": "86dcc07f-6aeb-4119-819d-fb067a1fbecf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd4036ca-3586-4053-8e0c-ad739c64e8af",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "91201a48-a3da-4dbe-b79e-b4229a1633ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd4036ca-3586-4053-8e0c-ad739c64e8af",
                    "LayerId": "94ca47b4-d256-4c5c-8a74-7e81cd7e181a"
                }
            ]
        },
        {
            "id": "bf43044d-fbae-465e-90c0-774fa8ef7486",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b7e8428-d0db-4fc6-acb6-5c80ca4140c3",
            "compositeImage": {
                "id": "ad75639f-3711-41dc-8e51-6345c63274e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bf43044d-fbae-465e-90c0-774fa8ef7486",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c885c31e-a47c-46f0-bbee-5ea5f2259fc5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf43044d-fbae-465e-90c0-774fa8ef7486",
                    "LayerId": "94ca47b4-d256-4c5c-8a74-7e81cd7e181a"
                }
            ]
        },
        {
            "id": "c1f4bae5-ff08-4298-a75d-afc12f82a60a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b7e8428-d0db-4fc6-acb6-5c80ca4140c3",
            "compositeImage": {
                "id": "a6741062-a481-401a-b630-1d504aaa8b0a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c1f4bae5-ff08-4298-a75d-afc12f82a60a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "66049d26-4c95-4452-a4e5-600fea320a04",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c1f4bae5-ff08-4298-a75d-afc12f82a60a",
                    "LayerId": "94ca47b4-d256-4c5c-8a74-7e81cd7e181a"
                }
            ]
        },
        {
            "id": "5591588b-3a0a-4714-94d3-b78bdb1f5376",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b7e8428-d0db-4fc6-acb6-5c80ca4140c3",
            "compositeImage": {
                "id": "56b12659-540c-4d00-ada5-8f4c25c88929",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5591588b-3a0a-4714-94d3-b78bdb1f5376",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "82642e49-98c1-49d1-978e-e147d9e0df84",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5591588b-3a0a-4714-94d3-b78bdb1f5376",
                    "LayerId": "94ca47b4-d256-4c5c-8a74-7e81cd7e181a"
                }
            ]
        },
        {
            "id": "10da509b-9c77-4b63-9486-50e07195e74f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b7e8428-d0db-4fc6-acb6-5c80ca4140c3",
            "compositeImage": {
                "id": "28ebeb47-b38b-4126-bcbd-5395cd2db160",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "10da509b-9c77-4b63-9486-50e07195e74f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "308fb29e-7d47-432f-92e6-38a97302fe57",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "10da509b-9c77-4b63-9486-50e07195e74f",
                    "LayerId": "94ca47b4-d256-4c5c-8a74-7e81cd7e181a"
                }
            ]
        },
        {
            "id": "dd756276-7fab-4b3c-9652-3c61a73a3378",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b7e8428-d0db-4fc6-acb6-5c80ca4140c3",
            "compositeImage": {
                "id": "e781d4f0-a3d0-43d6-869b-9e7437049548",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dd756276-7fab-4b3c-9652-3c61a73a3378",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "68d8f09d-daf1-4929-903c-ef93f7a4f091",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dd756276-7fab-4b3c-9652-3c61a73a3378",
                    "LayerId": "94ca47b4-d256-4c5c-8a74-7e81cd7e181a"
                }
            ]
        },
        {
            "id": "3c4fb1e7-85e8-4386-9d09-498d0724bb4c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b7e8428-d0db-4fc6-acb6-5c80ca4140c3",
            "compositeImage": {
                "id": "57f786f1-ec69-4571-a039-0c241f97ffe9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c4fb1e7-85e8-4386-9d09-498d0724bb4c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a519a2dc-ee6e-4cee-9534-bd49c1b78147",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c4fb1e7-85e8-4386-9d09-498d0724bb4c",
                    "LayerId": "94ca47b4-d256-4c5c-8a74-7e81cd7e181a"
                }
            ]
        }
    ],
    "gridX": 18,
    "gridY": 16,
    "height": 25,
    "layers": [
        {
            "id": "94ca47b4-d256-4c5c-8a74-7e81cd7e181a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6b7e8428-d0db-4fc6-acb6-5c80ca4140c3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 18,
    "xorig": 9,
    "yorig": 24
}