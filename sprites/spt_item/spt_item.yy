{
    "id": "12deaaf5-cb37-41f0-84b0-78cc0ffc217d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spt_item",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a7d77f9f-b72c-44ac-958d-94e043eb82d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "12deaaf5-cb37-41f0-84b0-78cc0ffc217d",
            "compositeImage": {
                "id": "19e19eff-3866-4696-8698-d0b2f903022f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a7d77f9f-b72c-44ac-958d-94e043eb82d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff74b246-d992-4593-a7a3-de649646c64b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a7d77f9f-b72c-44ac-958d-94e043eb82d2",
                    "LayerId": "d55a8df7-18c9-4d01-a2a3-6157d1d78491"
                }
            ]
        },
        {
            "id": "749c64f5-d1dc-418f-9318-05de1da771bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "12deaaf5-cb37-41f0-84b0-78cc0ffc217d",
            "compositeImage": {
                "id": "5ac650b8-6167-441c-a58c-4a1ab9558f64",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "749c64f5-d1dc-418f-9318-05de1da771bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e4f3bbd3-f9d3-45ed-96ec-e416a5ae3699",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "749c64f5-d1dc-418f-9318-05de1da771bf",
                    "LayerId": "d55a8df7-18c9-4d01-a2a3-6157d1d78491"
                }
            ]
        },
        {
            "id": "cd52ec31-c5a1-401a-8d90-7919f51e5f75",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "12deaaf5-cb37-41f0-84b0-78cc0ffc217d",
            "compositeImage": {
                "id": "231a28e5-7d80-4055-a9b4-c7a27cc58347",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd52ec31-c5a1-401a-8d90-7919f51e5f75",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7372b342-7484-471b-90be-7ccf478ea212",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd52ec31-c5a1-401a-8d90-7919f51e5f75",
                    "LayerId": "d55a8df7-18c9-4d01-a2a3-6157d1d78491"
                }
            ]
        },
        {
            "id": "7f34a880-4d6e-441c-8cbb-6629042d6a95",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "12deaaf5-cb37-41f0-84b0-78cc0ffc217d",
            "compositeImage": {
                "id": "b6ced2bb-d5d3-41c7-b6e9-b7ea6c9832d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f34a880-4d6e-441c-8cbb-6629042d6a95",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b1b42cb5-cd4f-40f6-82fe-a8f0759a518e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f34a880-4d6e-441c-8cbb-6629042d6a95",
                    "LayerId": "d55a8df7-18c9-4d01-a2a3-6157d1d78491"
                }
            ]
        },
        {
            "id": "e0c98c2d-80fa-4883-bcfa-8b26c23079d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "12deaaf5-cb37-41f0-84b0-78cc0ffc217d",
            "compositeImage": {
                "id": "d066902a-8090-46c3-be74-f9ab4e3f0d13",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e0c98c2d-80fa-4883-bcfa-8b26c23079d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45cd6f0e-abbe-4254-93ed-ed48dd25e42b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e0c98c2d-80fa-4883-bcfa-8b26c23079d7",
                    "LayerId": "d55a8df7-18c9-4d01-a2a3-6157d1d78491"
                }
            ]
        },
        {
            "id": "299438e1-d84c-42b3-820f-33c616592d78",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "12deaaf5-cb37-41f0-84b0-78cc0ffc217d",
            "compositeImage": {
                "id": "fb78ef31-77e6-448d-8e86-15d56d013145",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "299438e1-d84c-42b3-820f-33c616592d78",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f2eb687c-cbb7-4c66-b539-13dc3e84648f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "299438e1-d84c-42b3-820f-33c616592d78",
                    "LayerId": "d55a8df7-18c9-4d01-a2a3-6157d1d78491"
                }
            ]
        },
        {
            "id": "e9f1a790-8c47-4aa3-a26f-447e6378e3b1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "12deaaf5-cb37-41f0-84b0-78cc0ffc217d",
            "compositeImage": {
                "id": "25d6cdf1-88d4-4980-b7af-8a515619b898",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9f1a790-8c47-4aa3-a26f-447e6378e3b1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f7751b75-5905-4123-92bd-14d0bdf98993",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9f1a790-8c47-4aa3-a26f-447e6378e3b1",
                    "LayerId": "d55a8df7-18c9-4d01-a2a3-6157d1d78491"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "d55a8df7-18c9-4d01-a2a3-6157d1d78491",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "12deaaf5-cb37-41f0-84b0-78cc0ffc217d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}