if(sprite_index != noone)
	draw_sprite_ext(sprite_index, image_index, x, y, image_xscale, 1, 0, c_dkgray, 1);

draw_set_font(f_name);
draw_set_halign(fa_center);
draw_text(x, y - sprite_yoffset - 8, name);
draw_set_halign(fa_left);