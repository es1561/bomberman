{
    "id": "68619505-8135-4f8b-a41a-d3552f24bb33",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_instance",
    "eventList": [
        {
            "id": "13ead5bb-d989-4546-8c5d-a120b62ce4ca",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "68619505-8135-4f8b-a41a-d3552f24bb33"
        },
        {
            "id": "473e3bee-916f-4f7e-aae7-d1f5258d5914",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "68619505-8135-4f8b-a41a-d3552f24bb33"
        }
    ],
    "maskSpriteId": "da9c4d7f-6467-43ba-8493-d1b3d3e5f6f7",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "081b267b-c1c0-41cd-adca-ef9f8faf1609",
    "visible": true
}