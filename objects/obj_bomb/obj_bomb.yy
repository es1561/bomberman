{
    "id": "6ed12d99-e72d-4c47-b5b0-afe12e9dac50",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_bomb",
    "eventList": [
        {
            "id": "00489692-dadf-4b40-afc6-4fd5c62aea96",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6ed12d99-e72d-4c47-b5b0-afe12e9dac50"
        },
        {
            "id": "2693cc02-b69f-421c-b01c-6256f696656a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "6ed12d99-e72d-4c47-b5b0-afe12e9dac50"
        },
        {
            "id": "981c03f9-f062-4143-b5cf-5954bb374d9a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "6ed12d99-e72d-4c47-b5b0-afe12e9dac50"
        },
        {
            "id": "5849ae48-b2fc-48a5-9c34-fab80fb3a6f8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "6ed12d99-e72d-4c47-b5b0-afe12e9dac50"
        },
        {
            "id": "23abedf7-5258-4874-ae64-549dc8c6ea34",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "6ed12d99-e72d-4c47-b5b0-afe12e9dac50"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "3904518e-0437-4448-82c0-f11bf9801c6d",
    "visible": true
}