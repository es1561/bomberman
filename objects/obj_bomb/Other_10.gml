var fire;
	
fire = instance_create_layer(x, y, "Instances", obj_fire);
fire.sprite_index = spt_fire_base;
	
#region top
	var flag = false;

	for(var i = 0; !flag && i < range - 1; i++)
	{
		var aux = (i + 1) * 16;
		var xx = x div 16;
		var yy = (y - aux) div 16;
		var obj;
		
		if(global.map_grid[# xx, yy] == FREE)
		{
			fire = instance_create_layer(x, y - aux, "Instances", obj_fire);
			fire.sprite_index = spt_fire_mid_v;
			fire.f_fake = f_fake;
		}
		else
		{
			if(global.map_grid[# xx, yy] == BLOCK || global.map_grid[# xx, yy] == BOMB)
			{
				fire = instance_create_layer(x, y - aux, "Instances", obj_fire);
				fire.sprite_index = spt_fire_top_v;
				fire.f_fake = f_fake;
			}
			
			flag = true;
		}
	}
	
	var xx = x div 16;
	var yy = (y - range * 16) div 16;
	if(!flag && global.map_grid[# xx, yy] != WALL)
	{
		fire = instance_create_layer(x, y - range * 16, "Instances", obj_fire);
		fire.sprite_index = spt_fire_top_v;
		fire.f_fake = f_fake;
	}
#endregion
	
#region bottom
	var flag = false;
	
	for(var i = 0; !flag && i < range - 1; i++)
	{
		var aux = (i + 1) * 16;
		var xx = x div 16;
		var yy = (y + aux) div 16;
		var obj;
		
		if(global.map_grid[# xx, yy] == FREE)
		{
			fire = instance_create_layer(x, y + aux, "Instances", obj_fire);
			fire.sprite_index = spt_fire_mid_v;
			fire.image_yscale = -1;
			fire.f_fake = f_fake;
		}
		else
		{
			if(global.map_grid[# xx, yy] == BLOCK || global.map_grid[# xx, yy] == BOMB)
			{
				fire = instance_create_layer(x, y + aux, "Instances", obj_fire);
				fire.sprite_index = spt_fire_top_v;
				fire.image_yscale = -1;
				fire.f_fake = f_fake;
			}
			
			flag = true;
		}
	}
	
	var xx = x div 16;
	var yy = (y + range * 16) div 16;
	if(!flag && global.map_grid[# xx, yy] != WALL)
	{
		fire = instance_create_layer(x, y + range * 16, "Instances", obj_fire);
		fire.sprite_index = spt_fire_top_v;
		fire.image_yscale = -1;
		fire.f_fake = f_fake;
	}
#endregion
	
#region right
	var flag = false;

	for(var i = 0; !flag && i < range - 1; i++)
	{
		var aux = (i + 1) * 16;
		var xx = (x + aux) div 16;
		var yy = y div 16;
		var obj;
		
		if(global.map_grid[# xx, yy] == FREE)
		{
			fire = instance_create_layer(x + aux, y, "Instances", obj_fire);
			fire.sprite_index = spt_fire_mid_h;
			fire.f_fake = f_fake;
		}
		else
		{
			if(global.map_grid[# xx, yy] == BLOCK || global.map_grid[# xx, yy] == BOMB)
			{
				fire = instance_create_layer(x + aux, y, "Instances", obj_fire);
				fire.sprite_index = spt_fire_top_h;
				fire.f_fake = f_fake;
			}
			
			flag = true;
		}
	}
	
	var xx = (x + range * 16) div 16;
	var yy = y div 16;
	if(!flag && global.map_grid[# xx, yy] != WALL)
	{
		fire = instance_create_layer(x + range * 16, y, "Instances", obj_fire);
		fire.sprite_index = spt_fire_top_h;
		fire.f_fake = f_fake;
	}
#endregion
	
#region left
	var flag = false;

	for(var i = 0; !flag && i < range - 1; i++)
	{
		var aux = (i + 1) * 16;
		var xx = (x - aux) div 16;
		var yy = y div 16;
		var obj;
		
		if(global.map_grid[# xx, yy] == FREE)
		{
			fire = instance_create_layer(x - aux, y, "Instances", obj_fire);
			fire.sprite_index = spt_fire_mid_h;
			fire.image_xscale = -1;
			fire.f_fake = f_fake;
		}
		else
		{
			if(global.map_grid[# xx, yy] == BLOCK || global.map_grid[# xx, yy] == BOMB)
			{
				fire = instance_create_layer(x - aux, y, "Instances", obj_fire);
				fire.sprite_index = spt_fire_top_h;
				fire.image_xscale = -1;
				fire.f_fake = f_fake;
			}
			
			flag = true;
		}
	}
	
	var xx = (x - range * 16) div 16;
	var yy = y div 16;
	if(!flag && global.map_grid[# xx, yy] != WALL)
	{
		fire = instance_create_layer(x - range * 16, y, "Instances", obj_fire);
		fire.sprite_index = spt_fire_top_h;
		fire.image_xscale = -1;
		fire.f_fake = f_fake;
	}
#endregion
	
instance_destroy();