/// @description Insert description here
// You can write your code in this editor

if(f_fake && instance_exists(obj_player)) 
	obj_player.bomb++;
 
var fire = instance_create_layer(x + 8, y + 8 , "Instances", obj_fire);
fire.sprite_index = spt_fire_center;

//top
var i, flag = true;
var grid_x = x div 16;
for(i = 1 ; flag && i <= range - 1; i++)
{
	var grid_y = (y - (i * 16)) div 16;
	
	if(global.ds_grid[# grid_x, grid_y] == FREE)
	{
		var fire = instance_create_layer(x + 8, y - (i * 16) + 8, "Instances", obj_fire);
		fire.sprite_index = spt_fire_v;
	}
	else
		flag = false;
}

if(flag && global.ds_grid[# grid_x, (y - (i * 16)) div 16] == FREE)
{
	var fire = instance_create_layer(x + 8, y - (i * 16) + 8, "Instances", obj_fire);
	fire.sprite_index = spt_fire_side_v;
}

//down
var i, flag = true;
var grid_x = x div 16;
for(i = 1 ; flag && i <= range - 1; i++)
{
	var grid_y = (y + (i * 16)) div 16;
	
	if(global.ds_grid[# grid_x, grid_y] == FREE)
	{
		var fire = instance_create_layer(x + 8, y + (i * 16) + 8, "Instances", obj_fire);
		fire.sprite_index = spt_fire_v;
		fire.image_yscale = -1;
	}
	else
		flag = false;
}

if(flag && global.ds_grid[# grid_x, (y + (i * 16)) div 16] == FREE)
{
	var fire = instance_create_layer(x + 8, y + (i * 16) + 8, "Instances", obj_fire);
	fire.sprite_index = spt_fire_side_v;
	fire.image_yscale = -1;
}

//right
var i, flag = true;
var grid_y = y div 16;
for(i = 1 ; flag && i <= range - 1; i++)
{
	var grid_x = (x + (i * 16)) div 16;
	
	if(global.ds_grid[# grid_x, grid_y] == FREE)
	{
		var fire = instance_create_layer(x + (i * 16) + 8, y + 8, "Instances", obj_fire);
		fire.sprite_index = spt_fire_h;
	}
	else
		flag = false;
}

if(flag && global.ds_grid[# (x + (i * 16)) div 16, grid_y ] == FREE)
{
	var fire = instance_create_layer(x + (i * 16) + 8, y + 8, "Instances", obj_fire);
	fire.sprite_index = spt_fire_side_h;
}

//left
var i, flag = true;
var grid_y = y div 16;
for(i = 1 ; flag && i <= range - 1; i++)
{
	var grid_x = (x - (i * 16)) div 16;
	
	if(global.ds_grid[# grid_x, grid_y] == FREE)
	{
		var fire = instance_create_layer(x - (i * 16) + 8, y + 8, "Instances", obj_fire);
		fire.sprite_index = spt_fire_h;
		fire.image_xscale = -1;
	}
	else
		flag = false;
}

if(flag && global.ds_grid[# (x - (i * 16)) div 16, grid_y ] == FREE)
{
	var fire = instance_create_layer(x - (i * 16) + 8, y + 8, "Instances", obj_fire);
	fire.sprite_index = spt_fire_side_h;
	fire.image_xscale = -1;
}