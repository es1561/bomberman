 /// @description Insert description here
// You can write your code in this editor
#macro WALL 0
#macro FREE 1

depth = -100;

hp = 1;
range = 1;
bomb = 1;
spd = 1;

f_in_game = false;

global.ds_grid = ds_grid_create(19, 19);
ds_grid_clear(global.ds_grid, FREE);

for(var xx = 0; xx < 19; xx++)
{
	for(var yy = 0; yy < 19; yy++)
	{
		if(yy == 0 || yy == 18)
			global.ds_grid[# xx, yy] = WALL;
		if(xx == 0 || xx == 18)
			global.ds_grid[# xx, yy] = WALL;
	}
}

for(var xx = 2; xx < 18; xx+=2)
{
	for(var yy = 2; yy < 18; yy+=2)
	{
		global.ds_grid[# xx, yy] = WALL;
	}	
}