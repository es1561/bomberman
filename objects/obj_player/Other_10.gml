/// @description Insert description here
// You can write your code in this editor

var buffer = buffer_create(256, buffer_fixed, 1);

buffer_write(buffer, buffer_u8, msg_position);
buffer_write(buffer, buffer_u16, x);
buffer_write(buffer, buffer_u16, y);
buffer_write(buffer, buffer_u16, sprite_index);
buffer_write(buffer, buffer_u8, image_index);

network_send_packet(obj_client.socket_id, buffer, buffer_tell(buffer));