{
    "id": "3bf7e25d-b345-4394-9859-bf4d8d923a51",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_player",
    "eventList": [
        {
            "id": "e947bf44-f565-4cbf-b980-8176cf464ba1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "3bf7e25d-b345-4394-9859-bf4d8d923a51"
        },
        {
            "id": "8bad97f8-4fe3-4eec-aca6-c8100cf5f61e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3bf7e25d-b345-4394-9859-bf4d8d923a51"
        },
        {
            "id": "065a7d0e-1250-41e3-a066-1d5b6dc694f3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "3bf7e25d-b345-4394-9859-bf4d8d923a51"
        },
        {
            "id": "a88f59ec-888e-4d9a-9eff-31ebb810c43f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "3bf7e25d-b345-4394-9859-bf4d8d923a51"
        },
        {
            "id": "41df4836-97bd-4f0c-8f1d-024ff692f2db",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "3bf7e25d-b345-4394-9859-bf4d8d923a51"
        },
        {
            "id": "0b7c2cb3-db30-429f-bd03-4d20e1bdd8f1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 27,
            "eventtype": 9,
            "m_owner": "3bf7e25d-b345-4394-9859-bf4d8d923a51"
        },
        {
            "id": "4e20ef78-2d0f-4819-92e6-9da41afcf4cc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "3bf7e25d-b345-4394-9859-bf4d8d923a51"
        },
        {
            "id": "60f11e21-9a55-4259-9afb-8bbcc9f345d0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "3bf7e25d-b345-4394-9859-bf4d8d923a51"
        },
        {
            "id": "749445ef-bf06-41ba-8443-dfeb4cb3aa1e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "3bf7e25d-b345-4394-9859-bf4d8d923a51"
        },
        {
            "id": "2e1020c0-eae4-491c-9dd2-00605666a252",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 117,
            "eventtype": 9,
            "m_owner": "3bf7e25d-b345-4394-9859-bf4d8d923a51"
        }
    ],
    "maskSpriteId": "da9c4d7f-6467-43ba-8493-d1b3d3e5f6f7",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "081b267b-c1c0-41cd-adca-ef9f8faf1609",
    "visible": true
}