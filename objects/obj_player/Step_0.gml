if(f_in_game)
{
	if(hp > 0)
{
	 /// @description Insert description here
	// You can write your code in this editor

	var x_dir = keyboard_check(vk_right) - keyboard_check(vk_left);
	var y_dir = keyboard_check(vk_down) - keyboard_check(vk_up);
	var hspd = x_dir * 2;
	var vspd = y_dir * 2;

	if(x_dir != 0)
	{
		var side = ((x_dir == 1 ? bbox_right : bbox_left) + hspd) div 16;
		var top = bbox_top div 16;
		var bottom = bbox_bottom div 16;
		var yy = y div 16;
	
		sprite_index = spt_bomb_side;
		image_xscale = x_dir;
	
		if(global.ds_grid[# side, yy] == FREE && global.ds_grid[# side, top] == FREE 
								&& global.ds_grid[# side, bottom] == FREE)
			x += hspd;
		else
		{ 
			var aux = y mod 16;                                       

			if(global.ds_grid[# side, top] == FREE)
				y--;
			else if(global.ds_grid[# side, bottom] == FREE)
				y++;
		}
	}
	else if(y_dir != 0)
	{
		var side = ((y_dir == 1 ? bbox_bottom : bbox_top) + vspd) div 16;
		var right = bbox_right div 16;
		var left = bbox_left div 16;
		var xx = x div 16;
	
		sprite_index = y_dir == 1 ? spt_bomb_down : spt_bomb_up;
	
		if(global.ds_grid[# xx, side] == FREE && global.ds_grid[# right, side] == FREE 
								&& global.ds_grid[# left, side] == FREE)
			y += vspd;
		else
		{
			var aux = y mod 16;

			if(global.ds_grid[# left, side] == FREE)
				x--;
			else if(global.ds_grid[# right, side] == FREE)
				x++;
		}
	}
	else
		sprite_index = spt_bomb_idle;
	
	if(keyboard_check(vk_space) && bomb > 0 && !collision_point(x, y, obj_bomb, false, false))
	{
		event_user(1);
		/*
		var grid_x = x div 16;
		var grid_y = y div 16;
		var obj = instance_create_layer(grid_x * 16, grid_y * 16, "Instances", obj_bomb);
	
		obj.range = range;
		*/
		bomb--;
	}
	
	event_user(0);
}
else
	game_restart();
}