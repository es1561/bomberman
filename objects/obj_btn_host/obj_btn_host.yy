{
    "id": "776f552a-d8d3-42ba-835c-e2fc2c144aa5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_btn_host",
    "eventList": [
        {
            "id": "ae478c45-d453-4779-830b-fc541e811ebb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "776f552a-d8d3-42ba-835c-e2fc2c144aa5"
        },
        {
            "id": "df4a948a-e56f-4d2b-a690-90bb5c2d53d0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "776f552a-d8d3-42ba-835c-e2fc2c144aa5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "126f1fba-a6f4-4480-85ca-40fd045a0976",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "9d2c1231-9ffc-4fd1-aedd-e6f4dc98be71",
    "visible": true
}