{
    "id": "3698734a-b1ce-42f5-b069-cc3ecacdfd2b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_btn_join",
    "eventList": [
        {
            "id": "bd13196c-48c7-48ba-a25f-887796923d8e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3698734a-b1ce-42f5-b069-cc3ecacdfd2b"
        },
        {
            "id": "2d2a6ca5-53bf-4264-8dd0-266a9d3fcd59",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "3698734a-b1ce-42f5-b069-cc3ecacdfd2b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "126f1fba-a6f4-4480-85ca-40fd045a0976",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "9d2c1231-9ffc-4fd1-aedd-e6f4dc98be71",
    "visible": true
}