{
    "id": "9fd620bd-32bd-44ce-a9da-d2157172d429",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_name",
    "eventList": [
        {
            "id": "978a5283-cffa-45be-aa1f-aaec2bdad808",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9fd620bd-32bd-44ce-a9da-d2157172d429"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "bd14e4fc-c13e-46e1-8df0-886de683487a",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "33485ff6-eb04-4479-9284-822efba104c5",
    "visible": true
}