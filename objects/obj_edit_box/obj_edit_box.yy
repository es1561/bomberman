{
    "id": "bd14e4fc-c13e-46e1-8df0-886de683487a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_edit_box",
    "eventList": [
        {
            "id": "99777b03-d46d-4a66-9ad2-983f5dc6ca83",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "bd14e4fc-c13e-46e1-8df0-886de683487a"
        },
        {
            "id": "47f49e15-f6a3-4041-8c22-9243b7c6e5e7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "bd14e4fc-c13e-46e1-8df0-886de683487a"
        },
        {
            "id": "68113dea-fd39-4caa-8aee-f70698bf2ad8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "bd14e4fc-c13e-46e1-8df0-886de683487a"
        },
        {
            "id": "f7d2a9e2-bc2b-4713-ba9b-ffc7f4d7267e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 53,
            "eventtype": 6,
            "m_owner": "bd14e4fc-c13e-46e1-8df0-886de683487a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}