draw_self();
draw_set_halign(fa_middle);
draw_text(x, y - sprite_yoffset - 24, name);
draw_set_color(c_black);
if(focus)
	draw_text(x, y - 12, str + "|");
else
	draw_text(x, y - 12, str);
draw_set_color(c_white);
draw_set_halign(fa_left);