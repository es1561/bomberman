{
    "id": "0f8596a3-651d-4253-8ee6-e99e4d9101e4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_fire",
    "eventList": [
        {
            "id": "3ae1f066-f7cf-40e8-96f1-511afd2a48ab",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "0f8596a3-651d-4253-8ee6-e99e4d9101e4"
        },
        {
            "id": "dae43edd-e5eb-43df-9296-537537bedc56",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "6ed12d99-e72d-4c47-b5b0-afe12e9dac50",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "0f8596a3-651d-4253-8ee6-e99e4d9101e4"
        },
        {
            "id": "3593fe56-3db7-486f-881c-ab1fe487d917",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "3bf7e25d-b345-4394-9859-bf4d8d923a51",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "0f8596a3-651d-4253-8ee6-e99e4d9101e4"
        },
        {
            "id": "b14732ee-c5b1-4dd4-8c27-47e460b12a9b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0f8596a3-651d-4253-8ee6-e99e4d9101e4"
        },
        {
            "id": "63130f4e-2707-42f0-921d-3376f989c7e9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "52519cef-420a-4bc8-bbe5-8d1cac93a466",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "0f8596a3-651d-4253-8ee6-e99e4d9101e4"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}