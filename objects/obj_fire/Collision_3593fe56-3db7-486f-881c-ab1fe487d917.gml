if(!(other.f_immune) && !ds_map_exists(ds_hit, other.id) && image_index  < 3)
{
	with(other)
	{
		hp--;
		
		if(hp > 0)
		{
			f_immune = true;
			alarm[0] = room_speed * 2;
		}
	}
	
	ds_hit[? other.id] = true;
}