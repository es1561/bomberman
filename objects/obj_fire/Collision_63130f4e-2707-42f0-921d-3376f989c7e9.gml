if(!(other.f_destroy) && !ds_map_exists(ds_hit, other.id))
{
	var aux = f_fake;
	
	with(other)
	{
		f_fake = aux;
		f_destroy = true;
		sprite_index = spt_block_die;
		image_index = 0;
	}

	ds_hit[? other.id] = id;
}