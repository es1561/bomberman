{
    "id": "27251ec6-e34f-437c-9210-3dfdd19526a2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_btn_server",
    "eventList": [
        {
            "id": "2647c8f4-3096-4d1d-9d14-e5b015eddb61",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "27251ec6-e34f-437c-9210-3dfdd19526a2"
        },
        {
            "id": "a5c3b2fb-75d0-4586-86d9-7fd62ba8aa1e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "27251ec6-e34f-437c-9210-3dfdd19526a2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "126f1fba-a6f4-4480-85ca-40fd045a0976",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "04a42be5-7636-4ef9-89bb-28d4967ff21a",
    "visible": true
}