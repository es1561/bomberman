{
    "id": "ef71b3a7-9207-486d-9bb0-323f07f5cd80",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_server_list",
    "eventList": [
        {
            "id": "dee29cf8-beac-4527-8383-003c19a3fb47",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ef71b3a7-9207-486d-9bb0-323f07f5cd80"
        },
        {
            "id": "ce2c2ce2-f93a-4501-a7d4-3f8433c44f22",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 68,
            "eventtype": 7,
            "m_owner": "ef71b3a7-9207-486d-9bb0-323f07f5cd80"
        },
        {
            "id": "d6782973-0409-48b5-814a-c7c88186f555",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "ef71b3a7-9207-486d-9bb0-323f07f5cd80"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}