switch(image_index)
{
	case 0://fire
		with(other)
			range = clamp(range + 1, 1, 8);
	break;
	
	case 1://gold-fire
		with(other)
			range = 8;
	break;
	
	case 2://bomb
		with(other)
			bomb = clamp(bomb + 1, 1, 6);
	break;
	
	case 3://gold-bomb
		with(other)
			bomb = 6;
	break;
	
	case 4://shoes
		with(other)
			spd = clamp(spd + 0.2, 1, 2);
	break;
	
	case 5://gold-shoes
		with(other)
			spd = 2;
	break;
	
	case 6://hearth
		with(other)
			hp++;
	break;
}

instance_destroy();