{
    "id": "e165576f-cc5c-49ea-9ed4-175bad06e2f4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_item",
    "eventList": [
        {
            "id": "b748d670-72a9-4933-8587-ce0f0884cc87",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "0f8596a3-651d-4253-8ee6-e99e4d9101e4",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "e165576f-cc5c-49ea-9ed4-175bad06e2f4"
        },
        {
            "id": "01d735c2-7bb2-40ff-b6d8-abe4be8b9cde",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "3bf7e25d-b345-4394-9859-bf4d8d923a51",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "e165576f-cc5c-49ea-9ed4-175bad06e2f4"
        },
        {
            "id": "7b99130b-3ea8-40fb-995e-f12bcfe4697f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "68619505-8135-4f8b-a41a-d3552f24bb33",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "e165576f-cc5c-49ea-9ed4-175bad06e2f4"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "12deaaf5-cb37-41f0-84b0-78cc0ffc217d",
    "visible": true
}