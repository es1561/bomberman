{
    "id": "5dce1aa2-a88d-43ea-9f46-54752278b158",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_server",
    "eventList": [
        {
            "id": "5d604330-63d4-4b84-9232-b91169ac0201",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5dce1aa2-a88d-43ea-9f46-54752278b158"
        },
        {
            "id": "d20efba2-7009-4110-894b-74845022fa5b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "5dce1aa2-a88d-43ea-9f46-54752278b158"
        },
        {
            "id": "3cd84273-d3e8-4592-8bf7-c5d0a68104cc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "5dce1aa2-a88d-43ea-9f46-54752278b158"
        },
        {
            "id": "5ae99771-7c36-40d5-bf0c-4549f8e0b469",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 68,
            "eventtype": 7,
            "m_owner": "5dce1aa2-a88d-43ea-9f46-54752278b158"
        },
        {
            "id": "685c3957-7495-473f-8985-3cd1946ba238",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "5dce1aa2-a88d-43ea-9f46-54752278b158"
        },
        {
            "id": "6b636020-9716-4573-99f9-9b62e8d348d2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 116,
            "eventtype": 9,
            "m_owner": "5dce1aa2-a88d-43ea-9f46-54752278b158"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}