/// @description 
if(server_status == WAITING)
{
	var buffer = buffer_create(256, buffer_fixed, 1);
	
	buffer_write(buffer, buffer_u8, msg_start);
	
	with(obj_server_client)
	{
		if(f_in_game)
			network_send_packet(socket_id, buffer, buffer_tell(buffer));
	}
	
	server_status = IN_GAME;
}