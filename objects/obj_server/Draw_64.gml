/// @description Insert description here
// You can write your code in this editor

var n = instance_number(obj_server_client);

draw_set_color(c_white);
draw_text(0, 0, "Players: " + string(player_count));

for(var i = 0; i < n; i++)
{
	var obj = instance_find(obj_server_client, i);
	draw_text(16, 32 + (i * 16), obj.ip);
}

