/// @description Insert description here
// You can write your code in this editor
var buffer = buffer_create(256, buffer_fixed, 1);

randomize();

buffer_write(buffer, buffer_u8, msg_spawn_item);
buffer_write(buffer, buffer_u8, irandom(18));
buffer_write(buffer, buffer_u8, irandom(18));
buffer_write(buffer, buffer_u8, irandom(6));

with(obj_server_client)
	network_send_packet(socket_id, buffer, buffer_tell(buffer));
	
alarm[0] = room_speed div 2