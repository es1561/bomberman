/// @description Insert description here
// You can write your code in this editor
var type = async_load[? "type"];

switch(type)
{
	 case network_type_connect:
		var client = instance_create_layer(0, 0, 
					"Instances", obj_server_client);
					
		client.socket_id = async_load[? "socket"];
		client.ip = async_load[? "ip"];
		client.client_id = client_id;
		
		ds_client[? client_id] = client;
		
		client_id++;
		player_count++;
	 break;
	 
	 case network_type_disconnect:
		
		var buffer = buffer_create(256, buffer_fixed, 1);
		
		buffer_write(buffer, buffer_u8, msg_disconnect);
		
		
		with(obj_server_client)
			if(socket_id == async_load[? "socket"])
				instance_destroy();
			else
				network_send_packet(socket_id, buffer,
									buffer_tell(buffer));
		
		player_count--;
	 break;
	 
	 case network_type_data:
		scr_server(async_load[? "buffer"], async_load[? "id"]);
	 break;
}