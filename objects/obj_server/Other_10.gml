/// @description map

#macro FREE 0
#macro WALL 1
#macro BLOCK 2
#macro BOMB 3

map_cell_x = 464 div 16;
map_cell_y = 464 div 16;
map_grid = ds_grid_create(map_cell_x, map_cell_y);

ds_grid_clear(map_grid, FREE);

for(var yy = 0; yy < map_cell_y; yy++)
{
	for(var xx = 0; xx < map_cell_x; xx++)
	{
		if(yy == 0 || yy == map_cell_y - 1)
			map_grid[# xx, yy] = WALL;
		else if(xx == 0 || xx == map_cell_x - 1)
			map_grid[# xx, yy] = WALL;
	}
}

for(var yy = 2; yy < map_cell_y - 2; yy++)
{
	if((yy mod 2) == 0)
	{
		for(var xx = 2; xx < map_cell_x - 2; xx++)
		{
			if((xx mod 2) == 0)
				map_grid[# xx, yy] = WALL;
		}
	}
}

randomize();
for(var yy = 2; yy < map_cell_y - 2; yy++)
{
	for(var xx = 2; xx < map_cell_x - 2; xx++)
	{
		if(map_grid[# xx, yy] == FREE && irandom(3) == 0)
			map_grid[# xx, yy] = BLOCK;
	}
}
