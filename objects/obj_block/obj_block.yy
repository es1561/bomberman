{
    "id": "52519cef-420a-4bc8-bbe5-8d1cac93a466",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_block",
    "eventList": [
        {
            "id": "b013290a-e7b6-4f3a-b489-ffac9ab63a3f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "52519cef-420a-4bc8-bbe5-8d1cac93a466"
        },
        {
            "id": "a490758d-c49b-4792-bf78-9823c37776ac",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "52519cef-420a-4bc8-bbe5-8d1cac93a466"
        },
        {
            "id": "18eb8b4e-18db-43b7-94c9-cbfd42ddc6f1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "52519cef-420a-4bc8-bbe5-8d1cac93a466"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "de9fb237-d344-4277-9b75-0ae2fd23ab19",
    "visible": true
}