if(!f_fake)
{
	var buffer = buffer_create(256, buffer_fixed, 1);
	
	buffer_write(buffer, buffer_u8, msg_destroy_block);
	buffer_write(buffer, buffer_u16, x);
	buffer_write(buffer, buffer_u16, y);
	
	network_send_packet(obj_client.socket_id, buffer, buffer_tell(buffer));
}

