{
    "id": "126f1fba-a6f4-4480-85ca-40fd045a0976",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_button",
    "eventList": [
        {
            "id": "d75cbf09-a5b4-413b-939d-bfa59573a4f8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "126f1fba-a6f4-4480-85ca-40fd045a0976"
        },
        {
            "id": "37016aad-18d5-43fc-a838-de7626e83c47",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "126f1fba-a6f4-4480-85ca-40fd045a0976"
        },
        {
            "id": "937818d6-4cea-4a64-a3cb-f18db853ecbe",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "126f1fba-a6f4-4480-85ca-40fd045a0976"
        },
        {
            "id": "249d4232-21cc-4364-aea5-0f380f2f9899",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "126f1fba-a6f4-4480-85ca-40fd045a0976"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}